#region Copyright
/*
 * Copyright 2017 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;

namespace ClusterixN.QueryProcessing.Data
{
    public class QueryProcessConfig
    {
        public string ConnectionString { get; set; }
        public string DataDir { get; set; }
        public int BlockLength { get; set; }
        public bool SyncQueryDrop { get; set; } = false;
        public bool UseCompression { get; set; } = false;
        public bool IsMultiDB { get; set; } = false;
        public string[] MultiDBConnectionStrings { get; set; }

        public bool LoadDataParallel { get; set; }

        public bool SaveDataBeforeSort { get; set; }
        public string DataBeforeSortDir { get; set; }

        public string GetConnectionStringByHashNumber(int hashNumber)
        {
            if (!IsMultiDB) return ConnectionString;

            if (MultiDBConnectionStrings.Length <= hashNumber) 
                throw new ArgumentException("Индекс хеширования вне диапазона строк подключений");

            return MultiDBConnectionStrings[hashNumber];
        }
    }
}
