#region Copyright
/*
 * Copyright 2017 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

﻿using System;
using System.Collections.Generic;
using ClusterixN.Common;
using ClusterixN.Network.Interfaces;
using ClusterixN.Network.Packets;
using ClusterixN.Network.Packets.Base;
using ClusterixN.QueryProcessing.Data;
using ClusterixN.QueryProcessing.Services.Base;
using ClusterixN.QueryProcessing.Services.Interfaces;

namespace ClusterixN.QueryProcessing.Services
{
    class CustomQueryService : QueryProcessingServiceBase, IService
    {
        public CustomQueryService(ICommunicator client, QueryProcessConfig dbConfig) : base(client, dbConfig)
        {
            Client.SubscribeToPacket<GroupSqlCommandPacket>(GroupSqlCommandPacketReceivedHandler);
        }
        
        
        private void GroupSqlCommandPacketReceivedHandler(PacketBase packetBase)
        {
            var packet = packetBase as GroupSqlCommandPacket;
            if (packet == null) return;
            
            var results = new List<string>();

            if (Config.IsMultiDB)
            {
                foreach (var connectionString in Config.MultiDBConnectionStrings)
                {
                    var result = ExecuteSql(packet.Sql, connectionString);
                    if (!string.IsNullOrWhiteSpace(result))
                    {
                        results.Add(result);
                    }
                }
            }
            else
            {
                var result = ExecuteSql(packet.Sql, Config.ConnectionString);
                if (!string.IsNullOrWhiteSpace(result))
                {
                    results.Add(result);
                }
            }

            Client.Send(new GroupSqlResponsePacket()
            {
                Message = string.Join("\n",results),
                Success = results.Count == 0
            });
        }

        private string ExecuteSql(string sql, string connectionString)
        {
            var database = ServiceLocator.Instance.DatabaseService.GetDatabase(connectionString, Guid.Empty.ToString());
            return database.CustomCommandQuery(sql);
        }


        protected override void OnIsPausedChanged(bool isPaused)
        {
        }

    }
}
