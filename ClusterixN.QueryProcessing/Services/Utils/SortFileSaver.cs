﻿using ClusterixN.QueryProcessing.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ClusterixN.QueryProcessing.Services.Utils
{
    static class SortFileSaver
    {
        public static void SaveSortData(QueryProcessConfig Config, Guid relationId, int number, byte[] data)
        {
            if (Config.SaveDataBeforeSort)
            {
                if (!Directory.Exists(Config.DataBeforeSortDir)) Directory.CreateDirectory(Config.DataBeforeSortDir);

                using (var file = new FileStream($"{Config.DataBeforeSortDir}{Path.DirectorySeparatorChar}{relationId}_{number}.tbl", FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
                {
                    file.Seek(0, SeekOrigin.End);
                    if (data != null)
                    {
                        file.Write(data, 0, data.Length);
                    }
                }
            }
        }
    }
}
