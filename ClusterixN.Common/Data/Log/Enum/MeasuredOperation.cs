#region Copyright
/*
 * Copyright 2017 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

﻿namespace ClusterixN.Common.Data.Log.Enum
{
    public enum MeasuredOperation
    {
        // ReSharper disable once InconsistentNaming
        NOP,

        /// <summary>
        /// Ожидание начала работы
        /// </summary>
        WaitStart,

        /// <summary>
        /// Ожидание JOIN
        /// </summary>
        WaitJoin,

        /// <summary>
        /// Ожидание SORT
        /// </summary>
        WaitSort,

        /// <summary>
        /// Выполнение select
        /// </summary>
        ProcessingSelect,

        /// <summary>
        /// Выполнение join
        /// </summary>
        ProcessingJoin,

        /// <summary>
        /// Выполнение sort
        /// </summary>
        ProcessingSort,

        /// <summary>
        /// Хеширование данных
        /// </summary>
        HashData,

        /// <summary>
        /// Сохранение файлов на диск
        /// </summary>
        FileSave,

        /// <summary>
        /// Загрузка данных в БД
        /// </summary>
        LoadData,

        /// <summary>
        /// Индексирование
        /// </summary>
        Indexing,

        /// <summary>
        /// Передача данных по сети
        /// </summary>
        DataTransfer,

        /// <summary>
        /// Удаление данных из БД
        /// </summary>
        DeleteData,

        /// <summary>
        /// Время работы
        /// </summary>
        WorkDuration,

        /// <summary>
        /// Сжатие данных
        /// </summary>
        Compression,

        /// <summary>
        /// Восстановление данных
        /// </summary>
        Decompression,

        /// <summary>
        /// Приостановка работы
        /// </summary>
        Pause
    }
}
