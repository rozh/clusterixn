#region Copyright
/*
 * Copyright 2019 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

﻿using System;
using System.Collections.Generic;
using ClusterixN.Common.Interfaces;

namespace ClusterixN.Common.Data.Log
{
    public class SizeLogEvent : ILogEvent
    {
        public DateTime Time { get; set; }
        public string Module { get; set; }
        public Guid RelationId { get; set; }
        public string RelationName { get; set; }
        public long Size { get; set; }

        public Dictionary<object, object> GetLogEventProperties()
        {
            var result = new Dictionary<object, object>();

            foreach (var property in GetType().GetProperties())
            {
                result.Add(property.Name, property.GetValue(this, null));
            }

            return result;
        }
    }
}
