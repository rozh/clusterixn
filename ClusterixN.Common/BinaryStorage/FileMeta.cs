﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System.Linq;
using System.Xml.Serialization;
using ClusterixN.Common.Utils;

namespace ClusterixN.Common.BinaryStorage
{
    public class FileMeta : XmlSerializationBase<FileMeta>
    {
        public string[] Columns
        {
            get => _columns;
            set => _columns = value;
        }

        public ColumnType[] Types
        {
            get => _types;
            set
            {
                _types = value;
                UpdateCounters();
            }
        }

        public ColumnIndex[] Indexes
        {
            get => _indexes;
            set => _indexes = value;
        }

        public FileMeta()
        {
            Columns = new string[0];
            Indexes = new ColumnIndex[0];
            Types = new ColumnType[0];
        }

        [XmlIgnore]
        public int IntCount;
        [XmlIgnore]
        public int LongCount;
        [XmlIgnore]
        public int FloatCount;
        [XmlIgnore]
        public int DoubleCount;
        [XmlIgnore]
        public int BoolCount;
        [XmlIgnore]
        public int StringCount;
        [XmlIgnore]
        public int DateTimeCount;
        [XmlIgnore]
        public int DateCount;
        [XmlIgnore]
        public int TimeCount;
        

        private string[] _columns;
        private ColumnType[] _types;
        private ColumnIndex[] _indexes;

        private void UpdateCounters()
        {
            IntCount = Types.Count(x => x == ColumnType.INT);
            LongCount = Types.Count(x => x == ColumnType.LONG);
            FloatCount = Types.Count(x => x == ColumnType.FLOAT);
            DoubleCount = Types.Count(x => x == ColumnType.DOUBLE);
            BoolCount = Types.Count(x => x == ColumnType.BOOLEAN);
            StringCount = Types.Count(x => x == ColumnType.STRING);
            DateTimeCount = Types.Count(x => x == ColumnType.DATETIME);
            DateCount = Types.Count(x => x == ColumnType.DATE);
            TimeCount = Types.Count(x => x == ColumnType.TIME);
        }
    }

    public struct ColumnIndex
    {
        public int Index;
        public bool IsPrimary;

        public ColumnIndex(int index, bool isPrimary)
        {
            Index = index;
            IsPrimary = isPrimary;
        }
    }
}