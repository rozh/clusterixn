﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using LZ4;

namespace ClusterixN.Common.BinaryStorage
{
    public class TableBinaryReader : IDisposable
    {
        private readonly string _table;
        private FileMeta _meta;
        private readonly Stream _file;
        private bool _disposed;
        private byte[] _readBuffer;
        private int _readBufferIndex;
        private int _readBufferSize;

        public TableBinaryReader(string table, string db, bool useCompression)
        {
            _table = table.ToUpper();
            var path = Path.Combine(db, table + "_meta.xml");
            _meta = FileMeta.Load(path);

            _file = new FileStream(Path.Combine(db, table), FileMode.Open, FileAccess.Read, FileShare.None, 1024 * 128);

            if (useCompression)
            {
                _file = new LZ4Stream(_file, CompressionMode.Decompress);
            }

            _readBuffer = new byte[1024 * 1024];
        }

        public TableBinaryReader(string filePath, FileMeta meta, bool useCompression)
        {
            _meta = meta;
            _file = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 1024 * 128);

            if (useCompression)
            {
                _file = new LZ4Stream(_file, CompressionMode.Decompress);
            }

            _readBuffer = new byte[1024 * 1024];

        }

        public ref FileMeta Meta => ref _meta;


        public FileMeta GetMeta()
        {
            return _meta;
        }

        public string Table => _table;
        
        public IEnumerable<TableRow?> ReadTableRow()
        {
            TableRow? row;
            while ((row = DeserializeTableRow()) != null)
            {
                yield return row;
            }
        }

        public TableRow? ReadNextTableRow()
        {
            var row = DeserializeTableRow();
            return row;
        }
        
        private void ReadAheadBuffer()
        {
            if (_readBufferIndex == 0)
            {
                _readBufferSize = _file.Read(_readBuffer, 0, _readBuffer.Length);
                return;
            }

            Array.Copy(_readBuffer, _readBufferIndex, _readBuffer, 0, _readBuffer.Length - _readBufferIndex);
            var offset = _readBuffer.Length - _readBufferIndex;
            _readBufferSize = _file.Read(_readBuffer, offset, _readBuffer.Length - offset);
            if (_readBufferSize > 0) _readBufferSize += offset;
            _readBufferIndex = 0;
        }

        private int GetReadBufferLength()
        {
            return _readBufferSize - _readBufferIndex;
        }


        private TableRow? DeserializeTableRow()
        {
            if (GetReadBufferLength() == 0)
            {
                ReadAheadBuffer();
                if (GetReadBufferLength() == 0) return null;
            }

            var types = _meta.Types;
            var row = new TableRow(ref _meta);
            for (var i = 0; i < _meta.Columns.Length; i++)
            {
                ReadData(types[i], row, i);
            }
            
            return row;
        }

        private void ReadData(ColumnType type, TableRow tableRow, int column)
        {
            int readLength;
            int bufLen = GetReadBufferLength();
            switch (type)
            {
                case ColumnType.LONG:
                    readLength = sizeof(long) + 1;
                    if (bufLen < readLength) ReadAheadBuffer();
                    if (_readBuffer[_readBufferIndex] == 0)
                    {
                        tableRow.AddValue(BitConverter.ToInt64(_readBuffer, _readBufferIndex + 1), column);
                    }
                    else
                    {
                        tableRow.SetNull(column);
                    }

                    break;
                case ColumnType.INT:
                    readLength = sizeof(int) + 1;
                    if (bufLen < readLength) ReadAheadBuffer();
                    if (_readBuffer[_readBufferIndex] == 0)
                    {
                        tableRow.AddValue(BitConverter.ToInt32(_readBuffer, _readBufferIndex + 1), column);
                    }
                    else
                    {
                        tableRow.SetNull(column);
                    }
                    break;
                case ColumnType.FLOAT:
                    readLength = sizeof(float) + 1;
                    if (bufLen < readLength) ReadAheadBuffer();
                    if (_readBuffer[_readBufferIndex] == 0)
                    {
                        tableRow.AddValue(BitConverter.ToSingle(_readBuffer, _readBufferIndex + 1), column);
                    }
                    else
                    {
                        tableRow.SetNull(column);
                    }

                    break;
                case ColumnType.DOUBLE:
                    readLength = sizeof(double) + 1;
                    if (bufLen < readLength) ReadAheadBuffer();
                    if (_readBuffer[_readBufferIndex] == 0)
                    {
                        tableRow.AddValue(BitConverter.ToDouble(_readBuffer, _readBufferIndex + 1), column);
                    }
                    else
                    {
                        tableRow.SetNull(column);
                    }

                    break;
                case ColumnType.BOOLEAN:
                    readLength = sizeof(bool) + 1;
                    if (bufLen < readLength) ReadAheadBuffer();
                    if (_readBuffer[_readBufferIndex] == 0)
                    {
                        tableRow.AddValue(BitConverter.ToBoolean(_readBuffer, _readBufferIndex + 1), column);
                    }
                    else
                    {
                        tableRow.SetNull(column);
                    }

                    break;
                case ColumnType.STRING:
                    readLength = sizeof(int) + 1;
                    if (bufLen < readLength)
                    {
                        ReadAheadBuffer();
                        bufLen = GetReadBufferLength();
                    }
                    var len = BitConverter.ToInt32(_readBuffer, _readBufferIndex + 1);

                    if (_readBuffer[_readBufferIndex] == 0)
                    {
                        if (bufLen < len + readLength) ReadAheadBuffer();
                        tableRow.AddValue(Encoding.UTF8.GetString(_readBuffer, _readBufferIndex + readLength, len), column);
                    }
                    else
                    {
                        tableRow.SetNull(column);
                    }

                    readLength += len;

                    break;
                case ColumnType.DATE:
                case ColumnType.TIME:
                case ColumnType.DATETIME:
                    readLength = sizeof(long) + 1;
                    if (bufLen < readLength) ReadAheadBuffer();
                    if (_readBuffer[_readBufferIndex] == 0)
                    {
                        var ticks = BitConverter.ToInt64(_readBuffer, _readBufferIndex + 1);
                        tableRow.AddValue(DateTime.FromBinary(ticks), column);
                    }
                    else
                    {
                        tableRow.SetNull(column);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
            _readBufferIndex += readLength;
        }

        public void Dispose()
        {
            if (_disposed) return;
            _disposed = true;
            _file.Flush();
            _file.Dispose();
        }

        public long GetSize()
        {
            return _file.Length;
        }

        public string GetName(int i)
        {
            return _meta.Columns[i];
        }

        public string GetDataTypeName(int i)
        {
            return GetFieldType(i).ToString();
        }

        public Type GetFieldType(int i)
        {
            switch (_meta.Types[i])
            {
                case ColumnType.LONG:
                    return typeof(long);
                case ColumnType.INT:
                    return typeof(int);
                case ColumnType.FLOAT:
                    return typeof(float);
                case ColumnType.DOUBLE:
                    return typeof(double);
                case ColumnType.BOOLEAN:
                    return typeof(bool);
                case ColumnType.STRING:
                    return typeof(string);
                case ColumnType.DATE:
                case ColumnType.TIME:
                case ColumnType.DATETIME:
                    return typeof(DateTime);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}