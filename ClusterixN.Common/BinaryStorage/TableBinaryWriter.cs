﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using LZ4;

namespace ClusterixN.Common.BinaryStorage
{
    public class TableBinaryWriter : IDisposable
    {
        private string _table;
        private readonly string _db;
        private readonly bool _useCompression;
        private FileMeta _meta;
        private Stream _file;
        private bool _disposed;
        private string _metaPath;
        private string _tablePath;
        private byte[] _writeBuffer;
        private int _writeBufferIndex;

        public TableBinaryWriter(string table, string db, bool useCompression)
        {
            _table = table.ToUpper();
            _db = db;
            _useCompression = useCompression;
            Init(table, db);
        }

        public TableBinaryWriter(Stream outputStream, FileMeta meta)
        {
            _file = outputStream;
            _meta = meta;
            _writeBuffer = new byte[1024 * 1024];
        }

        private void Init(string table, string db)
        {
            _tablePath = Path.Combine(db, table);
            _metaPath = Path.Combine(db, table + "_meta.xml");
            _file = new FileStream(_tablePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None, 1024 * 1024);
            _file.Seek(0, SeekOrigin.End);

            if (_useCompression)
            {
                _file = new LZ4Stream(_file, CompressionMode.Compress, blockSize: 1024 * 1024);
            }

            _writeBuffer = new byte[1024 * 1024];

            if (File.Exists(_metaPath))
                _meta = FileMeta.Load(_metaPath);
        }

        public ref FileMeta Meta => ref _meta;

        public string FileName => _tablePath;

        public void SaveMeta(FileMeta meta)
        {
            meta.Save(_metaPath);
            _meta = meta;
        }

        public int WriteTableRow(TableRow row)
        {
            return SerializeTableRow(row);
        }

        private void FlushBuffer()
        {
            _file.Write(_writeBuffer, 0, _writeBufferIndex);
            _writeBufferIndex = 0;
        }

        private void WriteBuffer(byte b)
        {
            if (_writeBuffer.Length <= _writeBufferIndex + 1)
            {
                FlushBuffer();
            }

            _writeBuffer[_writeBufferIndex] = b;
            _writeBufferIndex++;
        }
        private void WriteBuffer(byte[] b)
        {
            if (_writeBuffer.Length <= _writeBufferIndex + b.Length)
            {
                FlushBuffer();
            }

            Array.Copy(b, 0, _writeBuffer, _writeBufferIndex, b.Length);

            _writeBufferIndex += b.Length;
        }

        private int SerializeTableRow(TableRow row)
        {
            //var columns = row;
            var types = _meta.Types;
            var size = 0;
            for (var i = 0; i < types.Length; i++)
            {
                size += WriteData(row, types[i], i);
                //if (columns[i] is byte[])
                //{
                //    WriteBuffer((byte[])columns[i]);
                //}
                //else
                //{
                //}
            }

            return size;
        }

        private int WriteData(TableRow row, ColumnType type, int column)
        {
            byte[] bytes;
            switch (type)
            {
                case ColumnType.LONG:
                    bytes = new byte[sizeof(long) + 1];
                    if (row.IsDBNull(column))
                    {
                        bytes[0] = 1;
                        Array.Copy(BitConverter.GetBytes(0L), 0, bytes, 1, sizeof(long));
                    }
                    else
                    {
                        Array.Copy(BitConverter.GetBytes(row.GetInt64(column)), 0, bytes, 1, sizeof(long));
                    }
                    break;
                case ColumnType.INT:
                    bytes = new byte[sizeof(int) + 1];
                    if (row.IsDBNull(column))
                    {
                        bytes[0] = 1;
                        Array.Copy(BitConverter.GetBytes(0), 0, bytes, 1, sizeof(int));
                    }
                    else
                    {
                        Array.Copy(BitConverter.GetBytes(row.GetInt32(column)), 0, bytes, 1, sizeof(int));
                    }
                    break;
                case ColumnType.FLOAT:
                    bytes = new byte[sizeof(float) + 1];
                    if (row.IsDBNull(column))
                    {
                        bytes[0] = 1;
                        Array.Copy(BitConverter.GetBytes(0f), 0, bytes, 1, sizeof(float));
                    }
                    else
                    {
                        Array.Copy(BitConverter.GetBytes(row.GetFloat(column)), 0, bytes, 1, sizeof(float));
                    }
                    break;
                case ColumnType.DOUBLE:
                    bytes = new byte[sizeof(double) + 1];
                    if (row.IsDBNull(column))
                    {
                        bytes[0] = 1;
                        Array.Copy(BitConverter.GetBytes(0d), 0, bytes, 1, sizeof(double));
                    }
                    else
                    {
                        Array.Copy(BitConverter.GetBytes(row.GetDouble(column)), 0, bytes, 1, sizeof(double));
                    }
                    break;
                case ColumnType.BOOLEAN:
                    bytes = new byte[sizeof(bool) + 1];
                    if (row.IsDBNull(column))
                    {
                        bytes[0] = 1;
                        Array.Copy(BitConverter.GetBytes(false), 0, bytes, 1, sizeof(bool));
                    }
                    else
                    {
                        Array.Copy(BitConverter.GetBytes(row.GetBoolean(column)), 0, bytes, 1, sizeof(bool));
                    }
                    break;
                case ColumnType.STRING:

                    if (row.IsDBNull(column))
                    {
                        bytes = new byte[sizeof(int) + 1];
                        bytes[0] = 1;
                    }
                    else
                    {
                        var str = row.GetString(column);
                        var strBytes = Encoding.UTF8.GetBytes(str);

                        bytes = new byte[strBytes.Length + sizeof(int) + 1];
                        Array.Copy(BitConverter.GetBytes(strBytes.Length), 0, bytes, 1, sizeof(int));
                        Array.Copy(strBytes, 0, bytes, 1 + sizeof(int), strBytes.Length);
                    }
                    break;
                case ColumnType.DATE:
                case ColumnType.TIME:
                case ColumnType.DATETIME:
                    bytes = new byte[sizeof(long) + 1];
                    if (row.IsDBNull(column))
                    {
                        bytes[0] = 1;
                        Array.Copy(BitConverter.GetBytes(DateTime.MinValue.ToBinary()), 0, bytes, 1, sizeof(long));
                    }
                    else
                    {
                        Array.Copy(BitConverter.GetBytes(row.GetDateTime(column).ToBinary()), 0, bytes, 1, sizeof(long));
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            WriteBuffer(bytes);
            return bytes.Length;
        }


        public void Rename(string tableName)
        {
            tableName = tableName.ToUpper();
            FlushBuffer();
            if (_file is FileStream stream)
            {
                stream.Flush(true);
            }
            else
            {
                _file.Flush();
            }
            _file.Dispose();
            var newTablePath = Path.Combine(_db, tableName);
            var newMetaPath = Path.Combine(_db, tableName + "_meta.xml");

            File.Move(_tablePath, newTablePath);
            File.Move(_metaPath, newMetaPath);

            Init(tableName, _db);
        }

        public void DropTable()
        {
            Dispose();

            if (File.Exists(_tablePath))
            {
                File.Delete(_tablePath);
            }

            if (File.Exists(_metaPath))
            {
                File.Delete(_metaPath);
            }
        }

        public void FlushData()
        {
            FlushBuffer();

            if (_file is FileStream stream)
            {
                stream.Flush(true);
            }
            else
            {
                _file.Flush();
            }
        }

        public void Dispose()
        {
            if (_disposed) return;

            _disposed = true;
            FlushData();

            _file.Dispose();
        }
    }
}