﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;

namespace ClusterixN.Common.BinaryStorage
{
    public readonly struct TableRow
    {
        private readonly FileMeta _meta;
        private readonly int[] _ints;
        private readonly long[] _longs;
        private readonly float[] _floats;
        private readonly double[] _doubles;
        private readonly string[] _strings;
        private readonly bool[] _booleans;
        private readonly DateTime[] _dateTimes;
        private readonly byte[] _columnMap;
        private readonly byte[] _indexes;

        enum ColumnTypeIndex : int
        {
            LONG = 0,
            INT = 1,
            FLOAT = 2,
            DOUBLE = 3,
            BOOLEAN = 4,
            STRING = 5,
            DATETIME = 6
        }

        public TableRow(ref FileMeta meta)
        {
            _meta = meta;
            _columnMap = new byte[meta.Columns.Length];
            _indexes = new byte[7];

            _ints = new int[meta.IntCount];
            _longs = new long[meta.LongCount];
            _floats = new float[meta.FloatCount];
            _doubles = new double[meta.DoubleCount];
            _strings = new string[meta.StringCount];
            _booleans = new bool[meta.BoolCount];
            _dateTimes = new DateTime[meta.DateTimeCount + meta.DateCount + meta.TimeCount];
            FieldCount = meta.Columns.Length;
        }

        public void SetValues(object[] columns)
        {

            for (int i = 0; i < columns.Length; i++)
            {
                if (columns[i] == null)
                {
                    _columnMap[i] = 255;
                    continue;
                }
                switch (_meta.Types[i])
                {
                    case ColumnType.LONG:
                        AddValue((long)columns[i], i);
                        break;
                    case ColumnType.INT:
                        AddValue((int)columns[i], i);
                        break;
                    case ColumnType.FLOAT:
                        AddValue((float)columns[i], i);
                        break;
                    case ColumnType.DOUBLE:
                        AddValue((double)columns[i], i);
                        break;
                    case ColumnType.BOOLEAN:
                        AddValue((bool)columns[i], i);
                        break;
                    case ColumnType.STRING:
                        AddValue((string)columns[i], i);
                        break;
                    case ColumnType.DATE:
                    case ColumnType.TIME:
                    case ColumnType.DATETIME:
                        AddValue((DateTime)columns[i], i);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void SetNull(int column)
        {
            _columnMap[column] = 255;
        }

        public void AddValue(long value, int column)
        {
            _longs[_indexes[(int)ColumnTypeIndex.LONG]] = value;
            _columnMap[column] = _indexes[(int)ColumnTypeIndex.LONG];
            _indexes[(int)ColumnTypeIndex.LONG]++;
        }

        public void AddValue(int value, int column)
        {
            _ints[_indexes[(int)ColumnTypeIndex.INT]] = value;
            _columnMap[column] = _indexes[(int)ColumnTypeIndex.INT];
            _indexes[(int)ColumnTypeIndex.INT]++;
        }

        public void AddValue(float value, int column)
        {
            _floats[_indexes[(int)ColumnTypeIndex.FLOAT]] = value;
            _columnMap[column] = _indexes[(int)ColumnTypeIndex.FLOAT];
            _indexes[(int)ColumnTypeIndex.FLOAT]++;
        }

        public void AddValue(double value, int column)
        {
            _doubles[_indexes[(int)ColumnTypeIndex.DOUBLE]] = value;
            _columnMap[column] = _indexes[(int)ColumnTypeIndex.DOUBLE];
            _indexes[(int)ColumnTypeIndex.DOUBLE]++;
        }
        public void AddValue(bool value, int column)
        {
            _booleans[_indexes[(int)ColumnTypeIndex.BOOLEAN]] = value;
            _columnMap[column] = _indexes[(int)ColumnTypeIndex.BOOLEAN];
            _indexes[(int)ColumnTypeIndex.BOOLEAN]++;
        }
        public void AddValue(string value, int column)
        {
            _strings[_indexes[(int)ColumnTypeIndex.STRING]] = value;
            _columnMap[column] = _indexes[(int)ColumnTypeIndex.STRING];
            _indexes[(int)ColumnTypeIndex.STRING]++;
        }
        public void AddValue(DateTime value, int column)
        {
            _dateTimes[_indexes[(int)ColumnTypeIndex.DATETIME]] = value;
            _columnMap[column] = _indexes[(int)ColumnTypeIndex.DATETIME];
            _indexes[(int)ColumnTypeIndex.DATETIME]++;
        }

        public object this[int index] => GetValue(index);

        public object this[string name] => GetValue(GetOrdinal(name));
        

        public string GetName(int i)
        {
            return _meta.Columns[i];
        }

        public string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public object GetValue(int i)
        {
            if (_columnMap[i] == 255) return null;

            switch (_meta.Types[i])
            {
                case ColumnType.LONG:
                    return _longs[_columnMap[i]];
                case ColumnType.INT:
                    return _ints[_columnMap[i]];
                case ColumnType.FLOAT:
                    return _floats[_columnMap[i]];
                case ColumnType.DOUBLE:
                    return _doubles[_columnMap[i]];
                case ColumnType.BOOLEAN:
                    return _booleans[_columnMap[i]];
                case ColumnType.STRING:
                    return _strings[_columnMap[i]];
                case ColumnType.DATE:
                case ColumnType.TIME:
                case ColumnType.DATETIME:
                    return _dateTimes[_columnMap[i]];
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public bool GetBoolean(int i)
        {
            return _booleans[_columnMap[i]];
        }

        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }

        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public char GetChar(int i)
        {
            throw new NotImplementedException();
        }

        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public short GetInt16(int i)
        {
            throw new NotImplementedException();
        }

        public int GetInt32(int i)
        {
            return _ints[_columnMap[i]];
        }

        public long GetInt64(int i)
        {
            return _longs[_columnMap[i]];
        }

        public float GetFloat(int i)
        {
            return _floats[_columnMap[i]];
        }

        public double GetDouble(int i)
        {
            return _doubles[_columnMap[i]];
        }

        public string GetString(int i)
        {
            return _strings[_columnMap[i]];
        }

        public decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        public DateTime GetDateTime(int i)
        {
            return _dateTimes[_columnMap[i]];
        }
        
        public bool IsDBNull(int i)
        {
            return _columnMap[i] == 255;
        }

        public int FieldCount { get; }
    }
}
