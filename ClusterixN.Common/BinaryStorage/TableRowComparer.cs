﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.Collections.Generic;

namespace ClusterixN.Common.BinaryStorage
{
    public class TableRowComparer : IComparer<TableRow?>, IComparer<TableRow>
    {
        private readonly FileMeta _leftFileMeta;
        private readonly FileMeta _rightFileMeta;
        private readonly ColumnIndex[] _otherIndexes;
        private readonly int _indexLen;
        private readonly ColumnIndex[] _indexes;
        private readonly ColumnType[] _types;

        public TableRowComparer(FileMeta leftFileMeta, FileMeta rightFileMeta)
        {
            _leftFileMeta = leftFileMeta;
            _rightFileMeta = rightFileMeta;

            _indexLen = _leftFileMeta.Indexes.Length;
            _indexes = _leftFileMeta.Indexes;
            _types = _leftFileMeta.Types;
            _otherIndexes = _rightFileMeta.Indexes;
        }

        public int Compare(TableRow? leftRow, TableRow? rightRow)
        {
            if (leftRow == null && rightRow == null) return 0;
            if (leftRow == null) return 1;
            if (rightRow == null) return -1;

            return Compare(leftRow.Value, rightRow.Value);
        }

        public int Compare(TableRow row, TableRow other)
        {
            var compareResult = 0;
            
            for (var i = 0; i < _indexLen; i++)
            {
                var index = _indexes[i].Index;
                var otherIndex = _otherIndexes[i].Index;

                if (!row.IsDBNull(index) && !other.IsDBNull(otherIndex))
                {
                    switch (_types[index])
                    {
                        case ColumnType.LONG:
                            compareResult = row.GetInt64(index).CompareTo(other.GetInt64(otherIndex));
                            break;
                        case ColumnType.INT:
                            compareResult = row.GetInt32(index).CompareTo(other.GetInt32(otherIndex));
                            break;
                        case ColumnType.FLOAT:
                            compareResult = row.GetFloat(index).CompareTo(other.GetFloat(otherIndex));
                            break;
                        case ColumnType.DOUBLE:
                            compareResult = row.GetDouble(index).CompareTo(other.GetDouble(otherIndex));
                            break;
                        case ColumnType.BOOLEAN:
                            compareResult = row.GetBoolean(index).CompareTo(other.GetBoolean(otherIndex));
                            break;
                        case ColumnType.STRING:
                            compareResult = string.CompareOrdinal(row.GetString(index), other.GetString(otherIndex));
                            break;
                        case ColumnType.DATE:
                        case ColumnType.TIME:
                        case ColumnType.DATETIME:
                            compareResult = row.GetDateTime(index).CompareTo(other.GetDateTime(otherIndex));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(other), _types[index], null);
                    }
                }
                else
                {
                    if (row.IsDBNull(index) && other.IsDBNull(otherIndex)) compareResult = 0;
                    if (!row.IsDBNull(index) && other.IsDBNull(otherIndex)) compareResult = 1;
                    if (row.IsDBNull(index) && !other.IsDBNull(otherIndex)) compareResult = -1;
                }

                if (compareResult != 0) return compareResult;
            }

            return compareResult;
        }
    }
}
