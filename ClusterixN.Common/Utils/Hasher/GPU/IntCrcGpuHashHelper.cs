#region Copyright
/*
 * Copyright 2021 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ClusterixN.Common.Utils.Hasher.GPU
{
    /// <summary>
    /// Хеширование данных на GPU
    /// </summary>
    // ReSharper disable once ClassNeverInstantiated.Global
    public class IntCrcGpuHashHelper : GpuHashHelperBase
    {
        [DllImport("gpuhash.dll", EntryPoint = "HashIntCRC")]
        private static extern void HashData(IntPtr handle, byte[] data, uint size, int[] keyCols,
            uint keyColsSize, int nodeCount, ref IntPtr hashedBlock, ref int lenght);

        /// <inheritdoc />
        protected override void HashDataWithGPU(IntPtr handle, byte[] data, uint size, int[] keyCols, uint keyColsSize, int nodeCount,
            ref IntPtr hashedBlock, ref int lenght)
        {
            HashData(handle, data, size, keyCols, keyColsSize, nodeCount, ref hashedBlock, ref lenght);
        }
    }
}
