#region Copyright
/*
 * Copyright 2021 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System.Collections.Generic;
using System.IO;
using ClusterixN.Common.Interfaces;

namespace ClusterixN.Common.Utils.Hasher
{
    public abstract class ByteHashHelperBase : HashHelperBase, IHasher
    {
        public List<byte[]> ProcessData(byte[] data, int nodeCount, int[] keys)
        {
            var buffers = new List<MemoryStream>(nodeCount);
            for (var i = 0; i < nodeCount; i++)
            {
                buffers.Add(new MemoryStream(data.Length / nodeCount));
            }

            long startPos = 0;
            long endPos = 0;
            var nodeNumber = 0;
            for (long i = 0; i < data.Length; i++)
            {
                if (data[i] == '\r' || data[i] == '\n')
                {
                    endPos = i;
                    if (endPos - startPos > 0)
                    {
                        var keyBytes = GetKeyBytes(data, startPos, endPos, keys);
                        nodeNumber = (int) (Hash(keyBytes) % nodeCount);
                        if (nodeNumber < 0) nodeNumber *= -1;
                        buffers[nodeNumber].Write(data, (int) startPos, (int) (endPos - startPos));
                        buffers[nodeNumber].WriteByte((byte) '\n');
                    }

                    startPos = endPos + 1;
                }
            }

            var result = new List<byte[]>(nodeCount);
            for (var i = 0; i < nodeCount; i++)
            {
                result.Add(buffers[i].ToArray());
                buffers[i].Dispose();
            }

            return result;
        }

        protected abstract uint Hash(byte[] data);

        private static byte[] GetKeyBytes(byte[] line, long start, long end, int[] keys)
        {
            var startPos = start;
            var endPos = start;
            var fieldNumber = 0;
            var keyData = new MemoryStream((int) (end - start));
            for (var i = start; i < end; i++)
            {
                if (line[i] == '|' || i == end - 1)
                {
                    endPos = i;

                    for (int j = 0; j < keys.Length; j++)
                    {
                        if (keys[j] == fieldNumber)
                        {
                            keyData.Write(line, (int) startPos, (int) (endPos - startPos + (i == end - 1 ? 1 : 0)));
                        }
                    }

                    startPos = endPos + 1;
                    fieldNumber++;
                }
            }

            return keyData.ToArray();
        }
    }
}
