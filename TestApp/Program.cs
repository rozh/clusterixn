﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using ClusterixN.Common.BinaryStorage;
using ClusterixN.Common.Data.Query.Relation;
using ClusterixN.Common.Utils.Compression;
using ClusterixN.Database.CustomJoin;
using MergeJoinEngine;

namespace TestApp
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            var sw = new Stopwatch();

            //var compression = new LZ4CompressionProvider();
            //var data = File.ReadAllBytes(@"D:\Clusterix\tpch\1G\tbl\nation.tbl");
            //sw.Start();
            //var result = compression.CompressBytes(data);
            //sw.Stop();
            //Console.WriteLine(data.Length);
            //Console.WriteLine(result.Length);
            //Console.WriteLine(sw.Elapsed);
            //Console.WriteLine(data.Length / sw.Elapsed.TotalSeconds);
            //Console.ReadLine();
            //return;

            //var merge = new MergeJoin(@"WorkingDirectory=C:\Users\roman\source\repos\clusterixn\TestApp\bin\db;SortChunkSize=1000000;MergeJoinDirectory=C:\Users\roman\source\repos\clusterixn\Database\CustomJoin\MergeJoin\MergeJoinDataLoader\bin\Release");
            //var data = File.ReadAllBytes(@"D:\Clusterix\tpch\1G\tbl\lineitem.tbl");
            //sw.Start();
            //var chunk = merge.ResultToBinaryFormat(new RelationSchema()
            //{
            //    Fields = new List<Field>()
            //    {
            //        new Field() { Name = "L_ORDERKEY", Params = "long" },
            //        new Field() { Name = "L_PARTKEY", Params = "long" },
            //        new Field() { Name = "L_SUPPKEY", Params = "long" },
            //        new Field() { Name = "L_LINENUMBER", Params = "int" },
            //        new Field() { Name = "L_QUANTITY", Params = "float" },
            //        new Field() { Name = "L_EXTENDEDPRICE", Params = "float" },
            //        new Field() { Name = "L_DISCOUNT", Params = "float" },
            //        new Field() { Name = "L_TAX", Params = "float" },
            //        new Field() { Name = "L_RETURNFLAG", Params = "string" },
            //        new Field() { Name = "L_LINESTATUS", Params = "string" },
            //        new Field() { Name = "L_SHIPDATE", Params = "date" },
            //        new Field() { Name = "L_COMMITDATE", Params = "date" },
            //        new Field() { Name = "L_RECEIPTDATE", Params = "date" },
            //        new Field() { Name = "L_SHIPINSTRUCT", Params = "string" },
            //        new Field() { Name = "L_SHIPMODE", Params = "string" },
            //        new Field() { Name = "L_COMMENT", Params = "string" },
            //    },
            //    Indexes = new List<Index>()
            //    {
            //        new Index(){ FieldNames = new List<string>(){"L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY"  }}
            //    }
            //}, data);

            //sw.Stop();
            //Console.WriteLine(sw.Elapsed);
            //GC.Collect();

            //File.WriteAllBytes(@"C:\Users\roman\source\repos\clusterixn\TestApp\bin\db\uncompressed.bin", chunk);

            //merge.DropTmpRealtions();
            //merge.DropTable("LINEITEM");

            //merge.CreateRelation("LINEITEM", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY", "L_LINENUMBER", "L_QUANTITY", "L_EXTENDEDPRICE", "L_DISCOUNT", "L_TAX", "L_RETURNFLAG", "L_LINESTATUS", "L_SHIPDATE", "L_COMMITDATE", "L_RECEIPTDATE", "L_SHIPINSTRUCT", "L_SHIPMODE", "L_COMMENT" },
            //    new[] { "long", "long", "long", "int", "float", "float", "float", "float", "string", "string", "date", "date", "date", "string", "string", "string" });
            //merge.AddPrimaryKey("LINEITEM", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY" });
            //
            //sw.Reset();
            //sw.Start();
            //
            //merge.LoadFile(@"D:\Clusterix\tpch\1G\tbl\lineitem.tbl", "LINEITEM");

            //merge.LoadFileBinaryFile(@"C:\Users\roman\source\repos\clusterixn\TestApp\bin\db\tmp.bin", "LINEITEM");

            //sw.Stop();
            //Console.WriteLine(sw.Elapsed);

            //Console.ReadLine();
            //return;



            var _db = new Database();
            _db.ConnectionString = @"UseCompression=0;WorkingDirectory=C:\Users\roman\source\repos\clusterixn\TestApp\bin\db;SortChunkSize=1000000;MergeJoinDirectory=C:\Users\roman\source\repos\clusterixn\Database\CustomJoin\MergeJoin\MergeJoinDataLoader\bin\Release";

            sw.Start();
            _db.CreateRelation("LINEITEM", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY", "L_LINENUMBER", "L_QUANTITY", "L_EXTENDEDPRICE", "L_DISCOUNT", "L_TAX", "L_RETURNFLAG", "L_LINESTATUS", "L_SHIPDATE", "L_COMMITDATE", "L_RECEIPTDATE", "L_SHIPINSTRUCT", "L_SHIPMODE", "L_COMMENT" },
                new[] { "long", "long", "long", "int", "float", "float", "float", "float", "string", "string", "date", "date", "date", "string", "string", "string" });
            _db.AddPrimaryKey("LINEITEM", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY" });
            //LoadFile(@"D:\Clusterix\tpch\1G\tbl\lineitem.tbl", "LINEITEM", @"C:\Users\roman\source\repos\clusterixn\TestApp\bin\db");
            _db.LoadFile(@"D:\Clusterix\tpch\1G\tbl\lineitem.tbl", "LINEITEM");
            sw.Stop();
            Console.WriteLine("Uncompressed load: {0}", sw.Elapsed);
            Console.ReadLine();
            


            _db.DropTmpRealtions();

            _db = new Database();
            _db.ConnectionString = @"UseCompression=1;WorkingDirectory=C:\Users\roman\source\repos\clusterixn\TestApp\bin\db;SortChunkSize=1000000;MergeJoinDirectory=C:\Users\roman\source\repos\clusterixn\Database\CustomJoin\MergeJoin\MergeJoinDataLoader\bin\Release";

            sw.Start();
            _db.CreateRelation("LINEITEM", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY", "L_LINENUMBER", "L_QUANTITY", "L_EXTENDEDPRICE", "L_DISCOUNT", "L_TAX", "L_RETURNFLAG", "L_LINESTATUS", "L_SHIPDATE", "L_COMMITDATE", "L_RECEIPTDATE", "L_SHIPINSTRUCT", "L_SHIPMODE", "L_COMMENT" },
                new[] { "long", "long", "long", "int", "float", "float", "float", "float", "string", "string", "date", "date", "date", "string", "string", "string" });
            _db.AddPrimaryKey("LINEITEM", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY" });
            //LoadFile(@"D:\Clusterix\tpch\1G\tbl\lineitem.tbl", "LINEITEM", @"C:\Users\roman\source\repos\clusterixn\TestApp\bin\db");
            _db.LoadFile(@"D:\Clusterix\tpch\1G\tbl\lineitem.tbl", "LINEITEM");
            sw.Stop();
            Console.WriteLine("Compressed load: {0}", sw.Elapsed);
            Console.ReadLine();
            //return;


            _db.DropTmpRealtions();
            //_db.DropTable("LINEITEM");
            //_db.DropTable("NATION");

            //var threadCount = 4;
            //
            //for (int i = 0; i < threadCount; i++)
            //{
            //    _db.CreateRelation("LINEITEM" + i, new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY", "L_LINENUMBER", "L_QUANTITY", "L_EXTENDEDPRICE", "L_DISCOUNT", "L_TAX", "L_RETURNFLAG", "L_LINESTATUS", "L_SHIPDATE", "L_COMMITDATE", "L_RECEIPTDATE", "L_SHIPINSTRUCT", "L_SHIPMODE", "L_COMMENT" },
            //        new[] { "long", "long", "long", "int", "float", "float", "float", "float", "string", "string", "date", "date", "date", "string", "string", "string" });
            //    _db.AddPrimaryKey("LINEITEM" + i, new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY" });
            //
            //}
            //_db.CreateRelation("NATION", new[] { "N_NATIONKEY", "N_NAME", "N_REGIONKEY", "N_COMMENT" }, new[] { "long", "string", "long", "string" });
            //_db.AddIndex("", "NATION", new[] { "N_REGIONKEY" });



            //sw.Start();

            //if (threadCount > 1)
            //{
            //    var tasks = new List<Task>();
            //    for (int i = 0; i < threadCount; i++)
            //    {
            //        tasks.Add(Task.Factory.StartNew(o =>
            //        {
            //            _db.LoadFile(@"D:\Clusterix\tpch\tools\dist\tpch3_dist\lineitem" + (int)o + ".tbl", "LINEITEM" + (int)o);
            //        }, i));
            //    }
            //
            //    Task.WaitAll(tasks.ToArray());
            //}
            //else
            //{
            //
            //    _db.LoadFile(@"D:\Clusterix\tpch\1G\tbl\lineitem.tbl", "LINEITEM" + 0);
            //}

            //_db.LoadFile(@"D:\Clusterix\tpch\1G\tbl\nation.tbl", "NATION");
            //sw.Stop();
            //Console.WriteLine(sw.Elapsed);
            //sw.Reset();

            Console.ReadLine();
        }

        public static void LoadFile(string filePath, string tableName, string dir)
        {
            var chunk = new LinkedList<string>();
            var chunk2 = new LinkedList<TableRow>();
            using (var input = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 1024 * 128))
            {
                using (var inputReader = new StreamReader(input, Encoding.UTF8, true, 1024 * 128))
                {
                    using (var reader = new TableBinaryReader(tableName, dir, false))
                    {
                        string line;
                        var i = 0;
                        while ((line = inputReader.ReadLine()) != null)
                        {
                            chunk.AddLast(line);
                            i++;
                            if (i > 1_000_000)
                            {
                                chunk.Clear();
                            }
                        }
                    }
                }
            }
        }

        private static IEnumerable<string> SplitString(string line)
        {
            var startIndex = 0;
            var offset = 0;
            if (line[0] == '"')
                offset = 1;

            for (var endIndex = 0; endIndex < line.Length; endIndex++)
            {
                if (line[endIndex] == '|')
                {
                    yield return line.Substring(startIndex + offset, endIndex - startIndex - offset);
                    startIndex = endIndex + 1;
                }
            }

            if (startIndex + offset < line.Length - offset)
            {
                yield return line.Substring(startIndex + offset, line.Length - startIndex - offset);
            }
        }

        private static TableRow ParseLine(string line, FileMeta meta)
        {
            //var parts = line.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim('"'));
            var types = meta.Types;
            var culture = CultureInfo.InvariantCulture;
            var row = new object[types.Length];
            var i = 0;
            foreach (var part in SplitString(line))
            {
                if (i == types.Length) break;

                switch (types[i])
                {
                    case ColumnType.LONG:
                        row[i] = Convert.ToInt64(part, culture);
                        break;
                    case ColumnType.INT:
                        row[i] = Convert.ToInt32(part, culture);
                        break;
                    case ColumnType.FLOAT:
                        row[i] = Convert.ToSingle(part, culture);
                        break;
                    case ColumnType.DOUBLE:
                        row[i] = Convert.ToDouble(part, culture);
                        break;
                    case ColumnType.BOOLEAN:
                        row[i] = Convert.ToBoolean(part);
                        break;
                    case ColumnType.STRING:
                        row[i] = part;
                        break;
                    case ColumnType.DATE:
                        var dp = part.Split('-');

                        row[i] = new DateTime(int.Parse(dp[0], 0, culture),
                            int.Parse(dp[1], 0, culture),
                            int.Parse(dp[2], 0, culture));

                        break;
                    case ColumnType.TIME:
                        row[i] = DateTime.ParseExact(part, "HH:mm:ss", culture);
                        break;
                    case ColumnType.DATETIME:
                        row[i] = Convert.ToDateTime(part, culture);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                i++;
            }

            return new TableRow(ref meta);
        }
    }
}
