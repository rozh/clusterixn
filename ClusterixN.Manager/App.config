﻿<?xml version="1.0" encoding="utf-8"?>

<configuration>
  
  <configSections>
    <section name="ListenersConfiguration"
             type="ClusterixN.Common.Configuration.Listener.ListenersConfigSection, ClusterixN.Common" />
    <section name="ConnectionsConfiguration"
             type="ClusterixN.Common.Configuration.Connection.ConnectionsConfigSection, ClusterixN.Common" />
    <section name="ClusterixN.Common"
             type="ClusterixN.Common.Configuration.ConfigurationElement.BaseServicesConfigurationSectionHandler, ClusterixN.Common" />
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog" />
  </configSections>

  <ClusterixN.Common>
    <add key="ClusterixN.Common.Interfaces.ILogService"
         value="ClusterixN.Infrastructure.Logging.NLogService, ClusterixN.Infrastructure.Logging" />
    <add key="ClusterixN.Common.Interfaces.IConfigurationService"
         value="ClusterixN.Common.Configuration.ConfigurationService, ClusterixN.Common" />
    <add key="ClusterixN.Common.Interfaces.IDatabaseService"
         value="ClusterixN.Database.MySQL.DatabaseService, ClusterixN.Database.MySQL" />
    <!--<add key="ClusterixN.Common.Interfaces.IQuerySourceManager"
         value="ClusterixN.Manager.Managers.QuerySourceManager, ClusterixN.Manager" />-->
    <add key="ClusterixN.Common.Interfaces.IQuerySourceManager"
         value="ClusterixN.Manager.Managers.HashQuerySourceManager, ClusterixN.Manager" />
    <!--<add key="ClusterixN.Common.Interfaces.IQuerySourceManager"
         value="ClusterixN.Manager.Managers.SortOnJoinQuerySourceManager, ClusterixN.Manager" />-->
    <add key="ClusterixN.Common.Interfaces.ICompressionProvider"
         value="ClusterixN.Common.Utils.Compression.GZipCompressionProvider, ClusterixN.Common" />
  </ClusterixN.Common>

  <nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        throwExceptions="true">
    <variable name="ApplicationDir" value="."></variable>
    <targets>
      <target name="logfile" xsi:type="File"
              fileName="debug_manager.log"
              layout="[${longdate}] ${level:uppercase=true} ${callsite:skipFrames=1} ${message} ${onexception:\n\rEXCEPTION OCCURRED:${exception:format=type,message,method:maxInnerExceptionLevel=5:innerFormat=shortType,message,method}}"
              archiveEvery="Day"
              archiveDateFormat="yyyyMMdd"
              archiveFileName="archive_manager_{#}.log"
              archiveNumbering="Date"
              encoding="utf-8"
              fileNameKind="Relative"
              maxArchiveFiles="14"
              keepFileOpen="false" />
      <target name="timelogfile" xsi:type="File"
              fileName="times_manager.log"
              layout="[${longdate}] ${message}"
              archiveEvery="Day"
              archiveDateFormat="yyyyMMdd"
              archiveFileName="archive_times_manager_{#}.log"
              archiveNumbering="Date"
              encoding="utf-8"
              fileNameKind="Relative"
              maxArchiveFiles="14"
              keepFileOpen="false" />
      <target name="droplogfile" xsi:type="File"
              fileName="dropqueries.log"
              layout="date=${longdate};${message}"
              archiveEvery="Day"
              archiveDateFormat="yyyyMMdd"
              archiveFileName="archive_dropqueries_{#}.log"
              archiveNumbering="Date"
              encoding="utf-8"
              fileNameKind="Relative"
              maxArchiveFiles="14"
              keepFileOpen="false" />
      <target name="console" xsi:type="ColoredConsole"
              layout="[${longdate}] ${level:uppercase=true} ${callsite:skipFrames=1} ${message} ${onexception:\n\rEXCEPTION OCCURRED:${exception:format=type,message,method:maxInnerExceptionLevel=5:innerFormat=shortType,message,method}}" />
      <target name="Database" xsi:type="Database" keepConnection="false"
              useTransactions="false"
              dbProvider="System.Data.SQLite.SQLiteConnection, System.Data.SQLite"
              connectionString="Data Source=timeLog.db;Version=3;Pooling=True;Max Pool Size=100;Cache Size=64000;Journal Mode=Off;Synchronous=Off;"
              commandText="INSERT INTO times(Timestamp, Module, Operation, `From`, `To`, QueryId, SubQueryId, RelationId, Duration) 
              VALUES(@Timestamp, @Module, @Operation, @From, @To, @QueryId, @SubQueryId, @RelationId, @Duration)">
        <parameter name="@Timestamp" layout="${event-properties:Time:format=yyyy-MM-dd HH\:mm\:ss.fff}"/>
        <parameter name="@Module" layout="${event-properties:Module}"/>
        <parameter name="@Operation" layout="${event-properties:Operation}"/>
        <parameter name="@From" layout="${event-properties:From}"/>
        <parameter name="@To" layout="${event-properties:To}"/>
        <parameter name="@QueryId" layout="${event-properties:QueryId}"/>
        <parameter name="@SubQueryId" layout="${event-properties:SubQueryId}"/>
        <parameter name="@RelationId" layout="${event-properties:RelationId}"/>
        <parameter name="@Duration" layout="${event-properties:Duration}"/>
      </target>
      <target name="PerfDatabase" xsi:type="Database" keepConnection="false"
              useTransactions="false"
              dbProvider="System.Data.SQLite.SQLiteConnection, System.Data.SQLite"
              connectionString="Data Source=performance.db;Version=3;Pooling=True;Max Pool Size=100;Cache Size=64000;Journal Mode=Off;Synchronous=Off;"
              commandText="INSERT INTO performance(Timestamp, Module, Counter, `Value`) 
              VALUES(@Timestamp, @Module, @Counter, @Value)">
        <parameter name="@Timestamp" layout="${event-properties:Time:format=yyyy-MM-dd HH\:mm\:ss.fff}"/>
        <parameter name="@Module" layout="${event-properties:Module}"/>
        <parameter name="@Counter" layout="${event-properties:Counter}"/>
        <parameter name="@Value" layout="${event-properties:Value}"/>
      </target>
    </targets>
    <rules>
      <logger name="defaultLogger" minlevel="Trace" writeTo="console" />
      <logger name="defaultLogger" minlevel="Trace" writeTo="logfile" />
      <logger name="timeLogger" minlevel="Trace" writeTo="Database" />
      <logger name="dropLogger" minlevel="Trace" writeTo="droplogfile" />
      <logger name="performanceLogger" minlevel="Trace" writeTo="PerfDatabase" />
      <logger name="mgmQueryTime" minlevel="Trace" writeTo="timelogfile" />
    </rules>
  </nlog>

  <ListenersConfiguration>
    <Listeners>
      <add name="DefaultListener" port="1234" />
    </Listeners>
  </ListenersConfiguration>

  <ConnectionsConfiguration>
    <Connections>
      <add name="DefaultServer" address="localhost" port="1234" />
    </Connections>
  </ConnectionsConfiguration>

  <startup>
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.0" />
  </startup>

  <appSettings>
    <add key="ModuleName" value="MGM_1"/>
    <add key="UseIntegratedJoin" value="0"/>
    <add key="MinRamAvaible" value="50" />
    <add key="RoutingMode" value="Relations" />
    <add key="SelectMode" value="AllNodesForOneQuery" /> <!-- CoreForRelation -->
    <add key="SelectQueriesCount" value="2" />
    <add key="JoinQueriesCount" value="2" /> <!-- 0 - без ограничений -->
    <add key="TimeoutMinutes" value="30" />
    <!--<add key="JoinDataStoreMode" value="Disc" />-->
    <add key="JoinDataStoreMode" value="Memory" />
    <add key="DataDir" value="tmp"/>
    <!--<add key="WorkMode" value="Centralized"/>-->
    <!--<add key="WorkMode" value="Decentralized"/>-->
    <add key="WorkMode" value="DecentralizedSelectJoin"/>
    <add key="UseCompression" value="0" />
  </appSettings>

</configuration>