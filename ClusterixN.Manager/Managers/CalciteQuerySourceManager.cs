#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.Collections.Generic;
using ClusterixN.Common.Data;
using ClusterixN.Common.Data.Query;
using ClusterixN.Common.Data.Query.Enum;
using ClusterixN.Common.Data.Query.Relation;
using ClusterixN.Common.Infrastructure.Base;
using ClusterixN.Common.Utils;
// ReSharper disable InconsistentNaming
// ReSharper disable RedundantEmptyObjectOrCollectionInitializer

namespace ClusterixN.Manager.Managers
{
    /// <summary>
    ///     Создает запросы на выполнение из теста TPC-H
    /// </summary>
    // ReSharper disable once UnusedMember.Global
    public class CalciteQuerySourceManager : QuerySourceManagerBase
    {
        #region Queries

        /// <summary>
        ///     Запрос № 1 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q1()
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        ///     Запрос № 2 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q2()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 3 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q3()
        {
            var builder = new QueryBuilder(3);

            #region SELECT

            var selectC = builder.AddSelectQuery(builder.CreateSelectQuery(
                @"SELECT
	C_CUSTKEY
FROM
	CUSTOMER
WHERE 
    C_MKTSEGMENT = 'MACHINERY'", 1));

            var selectO = builder.AddSelectQuery(builder.CreateSelectQuery(
                @"SELECT
	O_ORDERDATE,
	O_SHIPPRIORITY,
	O_ORDERKEY,
	O_CUSTKEY
FROM
	ORDERS
WHERE 
    O_ORDERDATE < DATE '1995-03-02';", 2));

            var selectL = builder.AddSelectQuery(builder.CreateSelectQuery(
                @"SELECT
	L_ORDERKEY,
	L_EXTENDEDPRICE,
	L_DISCOUNT
FROM
	LINEITEM
WHERE
	L_SHIPDATE > DATE '1995-03-02'", 3));

            #endregion

            #region JOIN

            var join1 = builder.AddJoinQuery(builder.CreateJoinQuery(@"SELECT
	O_ORDERDATE,
	O_SHIPPRIORITY,
	O_ORDERKEY
FROM
                                    " + Constants.LeftRelationNameTag + @" AS C,
                                    " + Constants.RightRelationNameTag + @" AS O
                        WHERE
                        C_CUSTKEY = O_CUSTKEY",
                builder.CreateRelationSchema(
                    new List<Field>()
                    {
                        new Field() {Name = "O_ORDERDATE", Params = "date"},
                        new Field() {Name = "O_SHIPPRIORITY", Params = "int"},
                        new Field() {Name = "O_ORDERKEY", Params = "long"},
                    },
                    new List<Index>()
                    {
                        new Index() {Name = "O_ORDERKEY", FieldNames = new List<string>() { "O_ORDERKEY"} },
                    })
                ,
                1,
                builder.CreateRelation(selectC, "C",
                    builder.CreateRelationSchema(
                        new List<Field>()
                        {
                            new Field() {Name = "C_CUSTKEY", Params = "long"},
                        },
                        new List<Index>()
                        {
                            new Index(true) {Name = "C_CUSTKEY", FieldNames = new List<string>() { "C_CUSTKEY"} },
                        })
                ),
                builder.CreateRelation(selectO, "O",
                    builder.CreateRelationSchema(
                        new List<Field>()
                        {
                            new Field() {Name = "O_ORDERDATE", Params = "date"},
                            new Field() {Name = "O_SHIPPRIORITY", Params = "int"},
                            new Field() {Name = "O_ORDERKEY", Params = "long"},
                            new Field() {Name = "O_CUSTKEY", Params = "long"},
                        },
                        new List<Index>()
                        {
                            new Index() {Name = "O_CUSTKEY", FieldNames = new List<string>() { "O_CUSTKEY"} },
                        }))));

            var join2 = builder.AddJoinQuery(builder.CreateJoinQuery(@"SELECT
	O_ORDERDATE,
	O_SHIPPRIORITY,
	L_ORDERKEY,
	L_EXTENDEDPRICE,
	L_DISCOUNT
FROM
                                    " + Constants.LeftRelationNameTag + @" AS J01,
                                    " + Constants.RightRelationNameTag + @" AS L
                        WHERE
                        L_ORDERKEY = O_ORDERKEY",
                builder.CreateRelationSchema(
                    new List<Field>()
                    {
                        new Field() {Name = "O_ORDERDATE", Params = " DATE NOT NULL"},
                        new Field() {Name = "O_SHIPPRIORITY", Params = " INT NOT NULL"},
                        new Field() {Name = "L_ORDERKEY", Params = " INT NOT NULL"},
                        new Field() {Name = "L_EXTENDEDPRICE", Params = " DECIMAL(15,2) NOT NULL"},
                        new Field() {Name = "L_DISCOUNT", Params = " DECIMAL(15,2) NOT NULL"},
                    },
                    new List<Index>()
                    {
                        new Index() {Name = "L_ORDERKEY", FieldNames = new List<string>() { "L_ORDERKEY"} },
                    })
                ,
                2,
                builder.CreateRelation(join1),
                builder.CreateRelation(selectL, "L",
                    builder.CreateRelationSchema(
                        new List<Field>()
                        {
                            new Field() {Name = "L_ORDERKEY", Params = "int"},
                            new Field() {Name = "L_EXTENDEDPRICE", Params = "float"},
                            new Field() {Name = "L_DISCOUNT", Params = "float"},
                        },
                        new List<Index>()
                        {
                            new Index() {Name = "L_ORDERKEY", FieldNames = new List<string>() { "L_ORDERKEY"} },
                        })
                ),
                @"SELECT
	O_ORDERDATE,
	O_SHIPPRIORITY,
	L_ORDERKEY,
	L_EXTENDEDPRICE,
	L_DISCOUNT
FROM
    " + Constants.RelationNameTag + @""));

            #endregion

            #region SORT

            builder.SetSortQuery(builder.CreateSortQuery(@"SELECT
	L_ORDERKEY,
	SUM(L_EXTENDEDPRICE * (1 - L_DISCOUNT)) AS REVENUE,
	O_ORDERDATE,
	O_SHIPPRIORITY
FROM
	" + Constants.RelationNameTag + @" AS J01
GROUP BY
	L_ORDERKEY,
	O_ORDERDATE,
	O_SHIPPRIORITY
ORDER BY
	REVENUE DESC,
	O_ORDERDATE;",
                builder.CreateRelationSchema(
                    new List<Field>()
                    {
                        new Field() {Name = "L_ORDERKEY", Params = " INT NOT NULL"},
                        new Field() {Name = "REVENUE", Params = " DECIMAL(18,4) NULL DEFAULT NULL"},
                        new Field() {Name = "O_ORDERDATE", Params = " DATE NOT NULL"},
                        new Field() {Name = "O_SHIPPRIORITY", Params = " INT NOT NULL"},
                    },
                    new List<Index>()
                    {
                    }),
                1,
                @"SELECT
	L_ORDERKEY,
	REVENUE,
	O_ORDERDATE,
	O_SHIPPRIORITY
FROM
	" + Constants.RelationNameTag + @";",
                builder.CreateRelation(join2, QueryRelationStatus.Wait)));
            
            #endregion
            
            return builder.GetQuery();
        }

        /// <summary>
        ///     Запрос № 4 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q4()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 5 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q5()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 6 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q6()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 7 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q7()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 8 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q8()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 9 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q9()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 10 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q10()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 11 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q11()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 12 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q12()
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        ///     Запрос № 13 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q13()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Запрос № 14 из теста TPC-H
        /// </summary>
        /// <returns>новый запрос на выполнение</returns>
        protected override Query Q14()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}