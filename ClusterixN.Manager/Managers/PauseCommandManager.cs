#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.Collections.Generic;
using ClusterixN.Common.Data.Enums;
using ClusterixN.Network.Interfaces;
using ClusterixN.Network.Packets;
using ClusterixN.Network.Packets.Data;

namespace ClusterixN.Manager.Managers
{
    internal class PauseCommandManager
    {
        private readonly IServerCommunicator _server;
        private Dictionary<Guid, Command> _lastCommands;
        private readonly object _syncObject = new object();

        public PauseCommandManager(IServerCommunicator server)
        {
            _server = server;
            _lastCommands = new Dictionary<Guid, Command>();
        }


        public void SendCommand(Command command, Guid nodeId)
        {
            lock (_syncObject)
            {

                if (_lastCommands.ContainsKey(nodeId))
                {
                    if (_lastCommands[nodeId] != command)
                    {
                        _lastCommands[nodeId] = command;
                        _server.Send(new CommandPacket()
                        {
                            Id = new Identify() { ClientId = nodeId },
                            Command = (int)command
                        });
                    }
                }
                else
                {
                    _server.Send(new CommandPacket()
                    {
                        Id = new Identify() { ClientId = nodeId },
                        Command = (int)command
                    });
                    _lastCommands.Add(nodeId, command);
                }
            }
        }

        public void PauseNode(Guid nodeId)
        {
            SendCommand(Command.Pause, nodeId);
        }

        public void ResumeNode(Guid nodeId)
        {
            SendCommand(Command.Resume, nodeId);
        }
    }
}