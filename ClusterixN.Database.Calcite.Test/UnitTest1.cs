﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ClusterixN.Database.Calcite.Antlr;

namespace ClusterixN.Database.Calcite.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var query =
                "SELECT      L_RETURNFLAG,      L_LINESTATUS,      L_QUANTITY,      L_EXTENDEDPRICE,      L_DISCOUNT,      L_TAX  FROM      LINEITEM  WHERE      L_SHIPDATE <= DATE '1998-12-01' - INTERVAL '90' DAY";
            var schema = "s";
            var rewriter = new CalciteQueryRewriter();
            var rewrittenQuery = rewriter.Rewrite(query, schema);
            Assert.AreEqual(rewrittenQuery,
                "SELECT      L_RETURNFLAG,      L_LINESTATUS,      L_QUANTITY,      L_EXTENDEDPRICE,      L_DISCOUNT,      L_TAX  FROM      s.LINEITEM  WHERE      L_SHIPDATE <= DATE '1998-12-01' - INTERVAL '90' DAY");
        }

        [TestMethod]
        public void TestMethod2()
        {
            var query =
                @"SELECT 
    L_RETURNFLAG, 
    L_LINESTATUS, 
    SUM(L_QUANTITY) AS SUM_QTY, 
    SUM(L_EXTENDEDPRICE) AS SUM_BASE_PRICE, 
    SUM(L_EXTENDEDPRICE * (1 - L_DISCOUNT)) AS SUM_DISC_PRICE, 
    SUM(L_EXTENDEDPRICE * (1 - L_DISCOUNT) * (1 + L_TAX)) AS SUM_CHARGE, 
    AVG(L_QUANTITY) AS AVG_QTY, 
    AVG(L_EXTENDEDPRICE) AS AVG_PRICE, 
    AVG(L_DISCOUNT) AS AVG_DISC, 
    COUNT(*) AS COUNT_ORDER 
FROM 
    r_c6f86ceea5674a8697569e9ab295b2fd_tmp 
GROUP BY 
    L_RETURNFLAG, 
    L_LINESTATUS 
ORDER BY 
    L_RETURNFLAG, 
    L_LINESTATUS;".ToUpper();
            var schema = "s";
            var rewriter = new CalciteQueryRewriter();
            var rewrittenQuery = rewriter.Rewrite(query, schema);
            Console.WriteLine(rewrittenQuery);
        }
    }
}
