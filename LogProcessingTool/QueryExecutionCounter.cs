#region Copyright
/*
 * Copyright 2018 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
 using System.Linq;
 using System.Text;
 using ClusterixN.Common.Data.Log.Enum;
 using LogProcessingTool.Data;
 using LogProcessingTool.Visualizer.Data;

namespace LogProcessingTool
{
    public class QueryExecutionCounter
    {
        private string _db;
        private SQLiteConnection _logDbConnection;

        private SQLiteConnection DbConnection
        {
            get
            {
                if (_logDbConnection == null)
                {
                    _logDbConnection = new SQLiteConnection($"Data Source={_db}; Version=3;");
                    _logDbConnection.Open();
                }
                return _logDbConnection;
            }
        }

        private List<QueryLogData> GetTimeLog(SQLiteConnection connection)
        {
            var result = new List<QueryLogData>();
            var cmd = connection.CreateCommand();
            cmd.CommandText =
                "SELECT Timestamp, Module, Operation, Duration, IsCanceled, q.Number as Number, q.Id as QueryId FROM times t LEFT JOIN query q ON t.QueryId = q.Id; ";
            var ignoredOperations = new HashSet<MeasuredOperation>()
            {
                MeasuredOperation.NOP,
                //MeasuredOperation.WaitStart,
                MeasuredOperation.WaitJoin,
                MeasuredOperation.WaitSort,
                MeasuredOperation.WaitStart,
                MeasuredOperation.WorkDuration
            };
            try
            {
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var canceled = 0;
                    if (!reader.IsDBNull(4))
                        int.TryParse(reader["IsCanceled"].ToString(), out canceled);

                    var log = new QueryLogData
                    {
                        Duration = double.Parse(reader["Duration"].ToString()),
                        Time = DateTime.Parse((string)reader["Timestamp"]),
                        Module = (string)reader["Module"],
                        IsCanceled = canceled == 1,
                        Operation =
                            (MeasuredOperation)Enum.Parse(typeof(MeasuredOperation), (string)reader["Operation"]),
                        QueryId = reader.IsDBNull(6) ? string.Empty : (string)reader["QueryId"],
                        QueryNumber = reader.IsDBNull(5) ? 0 : int.Parse(reader["Number"].ToString())
                    };
                    if (ignoredOperations.Contains(log.Operation)) continue;
                    result.Add(log);
                }
                reader.Close();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return result;
        }

        public string Count(string fileName)
        {
            _db = fileName;
            var message = "Подсчет времени обработки запросов...";
            ConsoleHelper.ReWriteLine(message);
            return Count(GetTimeLog(DbConnection));
        }

        private string Count(List<QueryLogData> logEvents)
        {
            var orderedLog = logEvents.OrderBy(l => l.Time);
            var result = new StringBuilder();
            var opResult = string.Empty;
            var queryTimes = new List<double>();
            var opTimes = new List<Tuple<MeasuredOperation, double>>();
            var runTimeByQuery = new Dictionary<int,QueryStat>();

            foreach (var groupedlog in orderedLog.GroupBy(l => l.QueryId))
            {
                var queryNumber = groupedlog.First().QueryNumber;
                if (!runTimeByQuery.ContainsKey(queryNumber))
                {
                    runTimeByQuery.Add(queryNumber, new QueryStat()
                    {
                        Number = queryNumber,
                    });
                }
                var stat = runTimeByQuery[queryNumber];
                var vizNodes = ConvertToVizNodes(groupedlog.ToList());
                var execTime = TimeSpan.Zero;
                foreach (var vizNode in vizNodes)
                {
                    foreach (var vizNodeOp in vizNode.VizData.GroupBy(v => v.Operation))
                    {
                        var time = ProjectDuration(vizNodeOp.ToList());
                        opResult += $"\t{vizNodeOp.Key}\tВремя: {time.TotalSeconds}{Environment.NewLine}";
                        opTimes.Add(new Tuple<MeasuredOperation, double>(vizNodeOp.Key, time.TotalSeconds));
                        stat.AddOperation(vizNodeOp.Key, time.TotalSeconds);
                    }
                    execTime += ProjectDuration(vizNode.VizData);
                }

                queryTimes.Add(execTime.TotalSeconds);
                stat.ExecTimes.Add(execTime.TotalSeconds);

                result.Append(
                //    $"{groupedlog.Key}\t{groupedlog.First().QueryNumber}\t{execTime.TotalSeconds}\n";
                $"Запрос: {groupedlog.Key}\tНомер: {queryNumber}\tВремя обработки: {execTime.TotalSeconds}{Environment.NewLine}");
                result.Append(opResult);
                opResult = string.Empty;
            }

            result.Append(Environment.NewLine);
            result.Append(Environment.NewLine);
            result.Append("Work avg");
            result.Append(Environment.NewLine);

            foreach (var g in opTimes.GroupBy(o=>o.Item1))
            {
                result.Append($"{g.Key}\t{g.Average(a=>a.Item2)}{Environment.NewLine}");
            }

            result.Append(Environment.NewLine);
            result.Append(Environment.NewLine);

            result.Append("Work sum");
            result.Append(Environment.NewLine);

            foreach (var g in opTimes.GroupBy(o => o.Item1))
            {
                result.Append($"{g.Key}\t{g.Sum(a => a.Item2)}{Environment.NewLine}");
            }

            result.Append(Environment.NewLine);
            var m = queryTimes.Average();
            result.Append($"M\t{m}{Environment.NewLine}");
            result.Append($"sig\t{Math.Sqrt(queryTimes.Select(q => Math.Pow(q - m, 2)).Average())}{Environment.NewLine}");

            result.Append(Environment.NewLine);
            result.Append($"Среднее{Environment.NewLine}");
            foreach (var qTime in runTimeByQuery)
            {
                result.Append(
                    //    $"{groupedlog.Key}\t{groupedlog.First().QueryNumber}\t{execTime.TotalSeconds}\n";
                    $"Запрос: {qTime.Key}\tСреднее время обработки: {qTime.Value.ExecTimes.Average()}{Environment.NewLine}");

                foreach (var operationTime in qTime.Value.OperationTimes)
                {
                    result.Append($"\t{operationTime.Key}\tВремя: {operationTime.Value.Average()}{Environment.NewLine}");
                }
            }

            return result.ToString();
        }

        
        private static List<VizNode> ConvertToVizNodes(List<QueryLogData> logEvents)
        {
            var orderedLog = logEvents.OrderBy(l => l.Time);
            var minDate = logEvents.Min(l => l.Time);
            var vizNodes = new List<VizNode>();

            foreach (var groupedlog in orderedLog.GroupBy(l => l.QueryId))
            {
                var vizNode = new VizNode() { Name = groupedlog.Key };
                foreach (var log in groupedlog)
                {
                    vizNode.VizData.Add(new VizData()
                    {
                        Start = log.Time - minDate,
                        Operation = log.Operation,
                        Duration = TimeSpan.FromMilliseconds(log.Duration),
                        IsCanceled = log.IsCanceled
                    });
                }
                vizNodes.Add(vizNode);
            }
            return vizNodes;
        }
        
        private TimeSpan ProjectDuration(List<VizData> data)
        {
            var lineLengths = new List<VizData>();
            var mergePerformed = false;

            foreach (var vizData in data.OrderBy(d=>d.Start))
            {
                var merged = false;
                if (lineLengths.Count>0)
                {
                    var len = lineLengths.Last();
                    if (len.Start + len.Duration >= vizData.Start &&
                        len.Start + len.Duration <= vizData.Start + vizData.Duration)
                    {
                        //продолжение вправо
                        len.Duration = vizData.Start - len.Start + vizData.Duration ;
                        merged = mergePerformed = true;
                    }
                    else if (len.Start <= vizData.Start &&
                        len.Start + len.Duration >= vizData.Start + vizData.Duration)
                    {
                        //измерение внутри промежутка
                        merged = mergePerformed = true;
                    }
                }
                if (!merged) lineLengths.Add(vizData);
            }
            if (!mergePerformed) return TimeSpan.FromSeconds(lineLengths.Sum(l => l.Duration.TotalSeconds));

            return ProjectDuration(lineLengths);
        }
    }

    public class QueryLogData : LogData
    {
        public int QueryNumber { get; set; }

        public string QueryId { get; set; }
    }
}
