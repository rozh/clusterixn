﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogProcessingTool {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class AnalyseQueries {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AnalyseQueries() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("LogProcessingTool.AnalyseQueries", typeof(AnalyseQueries).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на SELECT 
        ///	Number, 
        ///	AVG(DataTransfer) AS DataTransfer, 
        ///	AVG(LoadData) AS LoadData, 
        ///	AVG(ProcessingSelect) AS ProcessingSelect, 
        ///	AVG(ProcessingJoin) AS ProcessingJoin, 
        ///	AVG(ProcessingSort) AS ProcessingSort, 
        ///	AVG(FileSave ) AS FileSave, 
        ///	AVG(DeleteData) AS DeleteData
        ///FROM 
        ///	(SELECT 
        ///		Number, 
        ///		d1.Duration AS DataTransfer, 
        ///		d2.Duration AS LoadData, 
        ///		d3.Duration AS ProcessingSelect, 
        ///		d4.Duration AS ProcessingJoin, 
        ///		d5.Duration AS ProcessingSort, 
        ///		d6.Duration AS FileSave, 
        ///		d [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string AvgWorkByQuery {
            get {
                return ResourceManager.GetString("AvgWorkByQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на SELECT overal,canceled FROM
        ///(SELECT COUNT() as overal FROM query) as q1,
        ///(SELECT COUNT() as canceled FROM query WHERE IsCanceled=1) as q2;.
        /// </summary>
        internal static string CanceledQueryCount {
            get {
                return ResourceManager.GetString("CanceledQueryCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на SELECT Number, Count(Id) FROM query GROUP BY Number.
        /// </summary>
        internal static string QueryCountByQuery {
            get {
                return ResourceManager.GetString("QueryCountByQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на SELECT 
        ///	Number, 
        ///	SUM(DataTransfer) AS DataTransfer, 
        ///	SUM(LoadData) AS LoadData, 
        ///	SUM(ProcessingSelect) AS ProcessingSelect, 
        ///	SUM(ProcessingJoin) AS ProcessingJoin, 
        ///	SUM(ProcessingSort) AS ProcessingSort, 
        ///	SUM(FileSave) AS FileSave, 
        ///	SUM(DeleteData) AS DeleteData
        ///FROM 
        ///	(SELECT 
        ///		Number, 
        ///		d1.Duration AS DataTransfer, 
        ///		d2.Duration AS LoadData, 
        ///		d3.Duration AS ProcessingSelect, 
        ///		d4.Duration AS ProcessingJoin, 
        ///		d5.Duration AS ProcessingSort, 
        ///		d6.Duration AS FileSave, 
        ///		d7 [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string SumWorkByQuery {
            get {
                return ResourceManager.GetString("SumWorkByQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на SELECT Duration/1000 FROM times WHERE Operation = &quot;WorkDuration&quot;.
        /// </summary>
        internal static string WorkDuration {
            get {
                return ResourceManager.GetString("WorkDuration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на SELECT 
        ///	nodes.Module AS Node, 
        ///	d1.Duration AS DataTransfer, 
        ///	d2.Duration AS LoadData, 
        ///	d3.Duration AS ProcessingSelect, 
        ///	d4.Duration AS ProcessingJoin, 
        ///	d5.Duration AS ProcessingSort, 
        ///	d6.Duration AS FileSave, 
        ///	d7.Duration AS DeleteData 
        ///FROM
        ///	(SELECT DISTINCT Module FROM times) AS nodes
        ///	LEFT JOIN
        ///	(SELECT Module, SUM(Duration)/1000 AS Duration FROM times WHERE Operation = &quot;DataTransfer&quot; Group by Module, Operation) AS d1
        ///	ON nodes.Module = d1.Module
        ///	LEFT JOIN
        ///	(SELECT Module, SUM(Du [остаток строки не уместился]&quot;;.
        /// </summary>
        internal static string WorkStagesByNodes {
            get {
                return ResourceManager.GetString("WorkStagesByNodes", resourceCulture);
            }
        }
    }
}
