﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClusterixN.Common.Data.Log.Enum;

namespace LogProcessingTool.Data
{
    class OperationStat
    {
        public MeasuredOperation Operation { get; set; }
        public List<double> ExecTimes { get; set; }

        public OperationStat()
        {
            ExecTimes = new List<double>();
        }
    }
}
