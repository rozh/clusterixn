﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogProcessingTool.Data
{
    class DistributionMetric
    {
        public DistributionMetric(IEnumerable<RelationSizeEntity> relationSizes)
        {
            RelationSizeEntities = relationSizes.ToList();
        }

        public List<RelationSizeEntity> RelationSizeEntities { get; set; }

        public int UsedCores => RelationSizeEntities.Count(x => x.Size > 0);

        public double UsedCoresCoef => UsedCores / (double)RelationSizeEntities.Count;

        public double UsageDispersion => CalcDispersion();
        public double UsageSigma => Math.Sqrt(UsageDispersion);
        public double UsageSigmaCoef => Math.Sqrt(UsageDispersionCoef);
        public double UsageDispersionCoef => CalcNormalizedDispersion();
        public double DistributionCoef => CalcDistributionCoef();
        public double Metric => (UsedCoresCoef + (1-UsageSigmaCoef) + (1-DistributionCoef)) / 3;

        private double CalcDispersion()
        {
            var usedCores = RelationSizeEntities.Where(x => x.Size > 0).ToList();
            var m = usedCores.Average(x => x.Size);
            var sqSub = usedCores.Select(x => Math.Pow(x.Size - m, 2));
            return sqSub.Average();
        }
        
        private double CalcNormalizedDispersion()
        {
            var usedCores = RelationSizeEntities
                .Where(x => x.Size > 0)
                .Select(x => x.Size / (double) RelationSizeEntities.Max(r => r.Size))
                .ToList();
            var m = usedCores.Average();
            var sqSub = usedCores.Select(x => Math.Pow(x - m, 2));
            return sqSub.Average();
        }

        private double CalcDistributionCoef()
        {
            var dests = new int[RelationSizeEntities.Count];
            dests[0] = 0;
            for (int i = 1; i < RelationSizeEntities.Count; i++)
            {
                if (RelationSizeEntities[i].Size == 0)
                {
                    dests[i] = dests[i - 1] + 1;
                }
                else
                {
                    dests[i] = 0;
                }
            }

            return Math.Abs(dests.Sum() / ((0 + RelationSizeEntities.Count-1) *  (double)RelationSizeEntities.Count / 2));
        }
    }
}
