﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClusterixN.Common.Data.Log.Enum;

namespace LogProcessingTool.Data
{
    class QueryStat
    {
        public int Number { get; set; }
        public List<double> ExecTimes { get; set; }
        public Dictionary<MeasuredOperation, List<double>> OperationTimes { get; set; }

        public QueryStat()
        {
            ExecTimes = new List<double>();
            OperationTimes = new Dictionary<MeasuredOperation, List<double>>();
        }

        public void AddOperation(MeasuredOperation op, double time)
        {
            if (OperationTimes.ContainsKey(op)) OperationTimes[op].Add(time);
            else
            {
                OperationTimes.Add(op, new List<double>() {time});
            }
        }
    }
}
