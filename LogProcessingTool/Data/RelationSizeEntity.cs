﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogProcessingTool.Data
{
    public class RelationSizeEntity
    {
        public int Number { get; set; }
        public string RelationId { get; set; }
        public int CoreNumber { get; set; }
        public long Size { get; set; }
    }
}
