﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using LogProcessingTool.Data;

namespace LogProcessingTool.Visualizer
{
    public class DataDistributionMap : VisualizerBase
    {
        public int CoreSizePx = 10;

        public void Visualize(string db, string fileName)
        {
            var message = "Генерация графика распределения данных по ядрам...";
            ConsoleHelper.ReWriteLine(message);
            var logDbConnection = new SQLiteConnection($"Data Source={db}; Version=3;");
            logDbConnection.Open();
            Visualize(GetSizes(logDbConnection),fileName);
            ConsoleHelper.ReWriteLine(message + " готово.", true);
        }
        
        private void Visualize(List<RelationSizeEntity> logEvents, string fileName)
        {
            var image = Paint(logEvents);
            image.Save(fileName, ImageFormat.Png);
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            var mertics = CalculateMetrics(logEvents);
            File.WriteAllText(fileNameWithoutExtension+".txt", mertics);
        }
        
        protected List<RelationSizeEntity> GetSizes(SQLiteConnection connection)
        {
            var result = new List<RelationSizeEntity>();
            var cmd = connection.CreateCommand();
            cmd.CommandText =
                @"select  q.Id as Id, q.Number as Number, RelationId, RelationName, substr(RelationName, 40, round(length(RelationName)-39)) + (CASE Module
            WHEN ""JOIN_1"" THEN 0
            WHEN ""JOIN_2"" THEN 1
            WHEN ""JOIN_3"" THEN 2
            WHEN ""JOIN_4"" THEN 3
            WHEN ""JOIN_5"" THEN 4
            WHEN ""JOIN_6"" THEN 5
            WHEN ""NODE_1"" THEN 0
            WHEN ""NODE_2"" THEN 1
            WHEN ""NODE_3"" THEN 2
            WHEN ""NODE_4"" THEN 3
            WHEN ""NODE_5"" THEN 4
            WHEN ""NODE_6"" THEN 5
            ELSE 0
            END) * 12 as CoreNumber, Size
            from size s join relation r on s.RelationId = r.Id join subquery sq on r.SubQueryId = sq.Id join query q on sq.QueryId = q.Id
            where ""Module"" <> ""SORT_1"" and q.Id in (SELECT Id from query LIMIT 14)
            order by q.Number, Module, Timestamp
                ; ";
            try
            {
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new RelationSizeEntity()
                    {
                        Number = (int)(long)reader["Number"],
                        CoreNumber = (int)(long)reader["CoreNumber"],
                        Size = (long)reader["Size"],
                        RelationId = (string)reader["RelationId"],
                    });

                }
                reader.Close();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            return result;
        }

        private Bitmap Paint(List<RelationSizeEntity> log)
        {
            var satrtX = 50;
            var xoffset = satrtX;
            var width = (log.Max(x=>x.CoreNumber) + 1 )* CoreSizePx + xoffset;

            var yoffset = 0;
            var height = yoffset + log.GroupBy(x=>x.RelationId).Count() * CoreSizePx;
            var image = new Bitmap(width, height);
            var g = Graphics.FromImage(image);
            g.FillRectangle(new SolidBrush(Color.White), 0, 0, width, height);

            StringBuilder metrics = new StringBuilder();
            
            foreach (var sizeByQuery in log.GroupBy(x=>x.Number))
            {
                PaintLine(image, yoffset, 0, true, sizeByQuery.Key.ToString(), Color.Black);

                foreach (var sizeByRelation in sizeByQuery.GroupBy(x=>x.RelationId))
                {
                    var maxSize = sizeByRelation.Max(x => x.Size);
                    if (maxSize == 0)
                    {
                        maxSize = 1;
                        var color = Console.ForegroundColor;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Пустое отношение {sizeByRelation.Key}");
                        Console.ForegroundColor = color;
                    }
                    foreach (var size in sizeByRelation)
                    {
                        var color = GetColor(size.Size, 0, maxSize);
                        g.FillRectangle(new SolidBrush(color), xoffset, yoffset, CoreSizePx, CoreSizePx);
                        g.DrawRectangle(new Pen(Color.Black, 1), xoffset, yoffset, CoreSizePx, CoreSizePx);

                        xoffset += CoreSizePx;
                    }
                    yoffset += CoreSizePx;
                    xoffset = satrtX;
                }
            }

            return image;
        }
        private string CalculateMetrics(List<RelationSizeEntity> log)
        {
            StringBuilder metrics = new StringBuilder();
            
            foreach (var sizeByQuery in log.GroupBy(x=>x.Number))
            {
                foreach (var sizeByRelation in sizeByQuery.GroupBy(x=>x.RelationId))
                {
                    var maxSize = sizeByRelation.Max(x => x.Size);
                    if (maxSize == 0)
                    {
                        maxSize = 1;
                        var color = Console.ForegroundColor;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Пустое отношение {sizeByRelation.Key}");
                        Console.ForegroundColor = color;
                    }

                    var metric = new DistributionMetric(sizeByRelation);
                    metrics.AppendLine(
                        $"{sizeByQuery.Key}\t{sizeByRelation.Key}\t{metric.UsedCores}\t{metric.UsageSigma}\t{metric.UsedCoresCoef}\t{metric.UsageSigmaCoef}\t{metric.DistributionCoef}\t{metric.Metric}");

                }
            }

            return metrics.ToString();
        }

        private Color GetColor(long value, long min, long max)
        {
            var colors = new Color[] {Color.Red, Color.Yellow, Color.Green};

            if (value > max) value = max;
            if (value < min) value = min;

            var percent = (value-min)/(double)(max - min);

            return InterpolateColor(colors, percent);
        }
        private Color InterpolateColor(Color[] colors, double x)
        {
            double r = 0.0, g = 0.0, b = 0.0;
            var total = 0.0;
            var step = 1.0/(colors.Length - 1);
            var mu = 0.0;
            var sigma_2 = 0.035;

            foreach (var color in colors)
            {
                total += Math.Exp(-(x - mu)*(x - mu)/(2.0*sigma_2))/Math.Sqrt(2.0*Math.PI*sigma_2);
                mu += step;
            }

            mu = 0.0;
            foreach (var color in colors)
            {
                var percent = Math.Exp(-(x - mu)*(x - mu)/(2.0*sigma_2))/Math.Sqrt(2.0*Math.PI*sigma_2);
                mu += step;

                r += color.R*percent/total;
                g += color.G*percent/total;
                b += color.B*percent/total;
            }

            return Color.FromArgb((int)r, (int)g, (int)b);
        }

        public DataDistributionMap(float pixelPerMinute) : base(pixelPerMinute)
        {

        }
    }
}
