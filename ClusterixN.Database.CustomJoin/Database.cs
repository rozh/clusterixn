#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using ClusterixN.Common;
using ClusterixN.Common.Data.EventArgs;
using ClusterixN.Common.Interfaces;
using MergeJoinEngine;

namespace ClusterixN.Database.CustomJoin
{
    public class Database : IDatabase
    {
        protected readonly ILogger Logger;
        private MergeJoin _mergeJoin;
        private string _connectionString;

        public Database()
        {
            Logger = ServiceLocator.Instance.LogService.GetLogger("defaultLogger");

            _mergeJoin = new MergeJoin();
        }


        public string ConnectionString
        {
            get => _connectionString;
            set
            {
                _connectionString = value;
                _mergeJoin.ConnectionString = value;
            }
        }
        
        public void LoadFile(string filePath, string tableName)
        {
            var processStartInfo =
                new ProcessStartInfo(Path.Combine(ConnectionStringParser.GetMergeJoinDirectory(ConnectionString), "MergeJoinDataLoader.exe"), $"{ConnectionString} {filePath} {tableName}")
                {
                    CreateNoWindow = true,
                    RedirectStandardInput = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden
                };

            using (var process = Process.Start(processStartInfo))
            {
                process.WaitForExit();

                var message = process.StandardOutput.ReadToEnd();
                var error = process.StandardError.ReadToEnd();

                if (!string.IsNullOrWhiteSpace(message)) Logger.Trace(message);
                if (!string.IsNullOrWhiteSpace(error)) Logger.Error(error);
            }
        }

        public void DissableKeys(string tableName)
        {
            //throw new NotImplementedException();
        }

        public void EnableKeys(string tableName)
        {
            //throw new NotImplementedException();
        }

        public void DropTable(string tableName)
        {
            _mergeJoin.DropTable(tableName);
        }

        public void DropTmpRealtions()
        {
            _mergeJoin.DropTmpRealtions();
        }

        public void CreateRelation(string name, string[] fields, string[] types)
        {
            _mergeJoin.CreateRelation(name, fields, types);
        }

        public void AddIndex(string name, string relation, string[] fields)
        {
            _mergeJoin.AddIndex(name, relation, fields);
        }

        public void AddPrimaryKey(string relation, string[] fields)
        {
            _mergeJoin.AddPrimaryKey(relation, fields);
        }

        public void QueryIntoRelation(string relationName, string query)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Выборка по блокам из БД
        /// </summary>
        /// <param name="query">Запрос к БД</param>
        /// <param name="blockSize">Размер блока в строках</param>
        public void SelectBlocks(string query, int blockSize)
        {
            query = query.ToUpper();
#if DEBUG
            Logger.Trace(query);
#endif
            try
            {
                _mergeJoin.SelectBlocks(query, blockSize);
            }

            catch (Exception ex)
            {
                Logger.Error("Error:", ex);
                Logger.Trace($"Query: {query}");
            }
        }

        /// <summary>
        /// Получение данных из БД по блокам
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <param name="blockSize">Размер блока в строках</param>
        public List<byte[]> Select(string query, int blockSize)
        {
            throw new NotImplementedException();
        }

        public void ControlSelectBlocks(bool pause)
        {
            _mergeJoin.ControlSelectBlocks(pause);
        }

        /// <inheritdoc />
        public string CustomCommandQuery(string query)
        {
            throw new NotImplementedException();
        }

        #region Events

        public event EventHandler<SelectResultEventArg> BlockReaded
        {
            add => _mergeJoin.BlockReaded += value;
            remove => _mergeJoin.BlockReaded -= value;
        }


        #endregion

        public void StopSelectQuery()
        {
            _mergeJoin.StopSelectQuery();
        }

        public long GetRelationSize(string relationName)
        {
            return _mergeJoin.GetRelationSize(relationName);
        }

        public void Dispose()
        {
        }

    }
}