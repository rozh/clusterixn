#region Copyright
/*
 * Copyright 2021 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

﻿using System;
using System.Collections.Generic;
 using System.Diagnostics;
 using System.IO;
using System.Linq;
using System.Timers;
 using ClusterixN.Common;
 using ClusterixN.Common.Interfaces;
using ClusterixN.Common.Utils.Hasher;
using DataSplitter.Data.EventArgs;

namespace DataSplitter
{
    internal class SplitHelper : IDisposable
    {
        private readonly int _bufferLenght;
        private readonly byte[] _readBuffer;
        private readonly IHasher _hash;
        private readonly string _inFile;
        private readonly int _nodeCount;
        private readonly Timer _timer;
        private readonly Dictionary<string, FileStream> _fileStreams = new Dictionary<string, FileStream>();
        long _lineCount;
        private readonly Dictionary<int, long> _bufferCounter = new Dictionary<int, long>();

        private List<double> speeds = new List<double>();

        public double Speed
        {
            get { return speeds.Count > 0 ? speeds.Average() : 0; }
        }

        public SplitHelper(string filePath, int nodeCount, int bufferLenght = 1024*1024*64)
        {
            _inFile = filePath;
            _nodeCount = nodeCount;
            _bufferLenght = bufferLenght;
            _readBuffer = new byte[_bufferLenght];
            _hash = ServiceLocator.Instance.HashService;
            _timer = new Timer(500);
            _timer.Elapsed += ProcessingUpdate;
        }

        private void ProcessingUpdate(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            OnProcessingEvent(_lineCount);
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public Dictionary<int, long> Process(int[] keyIndexes, bool noWrite = false)
        {
            _timer.Start();
            speeds = new List<double>();

            var file = File.OpenRead(_inFile);
            var splitBuffer = new byte[_bufferLenght];
            var splitBufferLenght = 0;
            var bytesRead = 0;

            Stopwatch sw = new Stopwatch();

            while ((bytesRead = file.Read(_readBuffer,0,_bufferLenght)) != 0)
            {
                var linesCount = 0;
                var lastLineIndex = bytesRead-1;

                for (int i = 0; i < bytesRead; i++)
                {
                    if (_readBuffer[i] == '\n')
                    {
                        linesCount++;
                        lastLineIndex = i;
                    }
                }

                var hashBuffer = new byte[lastLineIndex + 1 + splitBufferLenght];
                if (splitBufferLenght > 0)
                {
                    Array.Copy(splitBuffer, hashBuffer, splitBufferLenght);
                }

                Array.Copy(_readBuffer,0, hashBuffer, splitBufferLenght, lastLineIndex + 1);
                
                splitBufferLenght = bytesRead - lastLineIndex - 1;
                if (splitBufferLenght > 0)
                    Array.Copy(_readBuffer,lastLineIndex+1, splitBuffer, 0, splitBufferLenght);
                sw.Start();
                var hashedData = _hash.ProcessData(hashBuffer, _nodeCount, keyIndexes);
                sw.Stop();
                speeds.Add(hashBuffer.Length / 1024.0 / 1024 / sw.Elapsed.TotalSeconds);
                sw.Reset();
                Write(_inFile, hashedData, noWrite);
                _lineCount+= linesCount;
            }

            //last line
            if (splitBufferLenght > 0)
            {
                var hashBuffer = new byte[splitBufferLenght];
                Array.Copy(splitBuffer,0, hashBuffer, 0, splitBufferLenght);
                var hashedData = _hash.ProcessData(hashBuffer, _nodeCount, keyIndexes);
                Write(_inFile, hashedData, noWrite);
                _lineCount++;
            }
            
            _timer.Stop();
            OnProcessingEvent(_lineCount);
            FlushBuffer();
            OnProcessingCompleteEvent(_lineCount, _bufferCounter);
            return _bufferCounter;
        }


        private void Write(string filePath, List<byte[]> hashedData, bool noWrite = false)
        {
            for (int i = 0; i < hashedData.Count; i++)
            {
                if (hashedData.Count == 0) continue;

                if (!noWrite)
                {
                    var filename = filePath + "_" + i;
                    if (!_fileStreams.ContainsKey(filename))
                    {
                        _fileStreams.Add(filename, new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write));
                    }

                    var file = _fileStreams[filename];

                    file.Write(hashedData[i], 0, hashedData[i].Length);
                }

                var chunk = hashedData[i];
                var linesCount = 0;
                for (int j = 0; j < chunk.Length; j++)
                {
                    if (chunk[j] == '\n')
                    {
                        linesCount++;
                    }
                }

                if (!_bufferCounter.ContainsKey(i))
                {
                    _bufferCounter.Add(i, linesCount);
                }
                else
                {
                    _bufferCounter[i] += linesCount;
                }
            }
        }

        private void FlushBuffer()
        {
            foreach (var fileStream in _fileStreams)
            {
                fileStream.Value.Flush(true);
                fileStream.Value.Close();
                fileStream.Value.Dispose();
            }

            _fileStreams.Clear();
        }

        public event EventHandler<ProcessingEventArgs> ProcessingEvent;
        public event EventHandler<ProcessingCompleteEventArgs> ProcessingCompleteEvent;

        protected virtual void OnProcessingEvent(long count)
        {
            ProcessingEvent?.BeginInvoke(this, new ProcessingEventArgs {ProcessedLines = count}, null, null);
        }

        protected virtual void OnProcessingCompleteEvent(long count, Dictionary<int, long> linesByNode)
        {
            ProcessingCompleteEvent?.Invoke(this,
                new ProcessingCompleteEventArgs {ProcessedLines = count, ProcessedLinesByNodes = linesByNode});
        }
    }
}