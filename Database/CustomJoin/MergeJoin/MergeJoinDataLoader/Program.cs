﻿using MergeJoinEngine;

namespace MergeJoinDataLoader
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var connectionString = args[0];
            var filePath = args[1];
            var tableName = args[2];


            var mergeJoin = new MergeJoin(connectionString);
            mergeJoin.LoadFile(filePath, tableName);
        }
    }
}
