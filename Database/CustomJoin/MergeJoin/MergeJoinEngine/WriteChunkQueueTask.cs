﻿using System;
using System.Collections.Generic;
using System.IO;
using ClusterixN.Common.BinaryStorage;

namespace MergeJoinEngine
{
    internal class WriteChunkQueueTask
    {
        public string ChunkName { get; set; }
        public string Dir { get; set; }

        public TableBinaryReader Reader { get; }
        public TableBinaryWriter MemoryWriter { get; }

        private MemoryStream _stream;
        private List<ChunkPointer> _chunkPointers;
        private int _offset;
        public WriteChunkQueueTask(int chunkSize, TableBinaryReader reader)
        {
            _stream = new MemoryStream();
            _chunkPointers = new List<ChunkPointer>(chunkSize);
            Reader = reader;
            MemoryWriter = new TableBinaryWriter(_stream, reader.Meta);
        }

        public void AddRow(TableRow row)
        {
            var size = MemoryWriter.WriteTableRow(row);
            var keys = new long[Reader.Meta.Indexes.Length];
            for (int i = 0; i < keys.Length; i++)
            {
                var colIndex = Reader.Meta.Indexes[i].Index;
                switch (Reader.Meta.Types[colIndex])
                {
                    case ColumnType.LONG:
                        keys[i] = row.GetInt64(colIndex);
                        break;
                    case ColumnType.INT:
                        keys[i] = row.GetInt32(colIndex);
                        break;
                    case ColumnType.FLOAT:
                        break;
                    case ColumnType.DOUBLE:
                        break;
                    case ColumnType.BOOLEAN:
                        break;
                    case ColumnType.STRING:
                        break;
                    case ColumnType.DATE:
                        break;
                    case ColumnType.TIME:
                        break;
                    case ColumnType.DATETIME:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            _chunkPointers.Add(new ChunkPointer(){Length = size, Offset = _offset, Keys = keys});

            _offset += size;
        }

        public byte[] GetChunkData()
        {
            MemoryWriter.FlushData();

            return _stream.ToArray();
        }

        public List<ChunkPointer> ChunkPointers => _chunkPointers;

        public void Clear()
        {
            MemoryWriter.Dispose();

            GC.Collect(GC.GetGeneration(_chunkPointers));
        }
    }

    public struct ChunkPointer
    {
        public int Offset;
        public int Length;

        public long[] Keys;
    }

    public class ChunkPointerComparer : IComparer<ChunkPointer>
    {
        public int Compare(ChunkPointer row, ChunkPointer other)
        {
            var compareResult = 0;

            for (var i = 0; i < row.Keys.Length; i++)
            {
                compareResult = row.Keys[i].CompareTo(other.Keys[i]);
                if (compareResult != 0) return compareResult;
            }

            return compareResult;
        }
    }
}
