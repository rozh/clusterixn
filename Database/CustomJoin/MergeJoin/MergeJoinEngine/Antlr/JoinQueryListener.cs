﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using Antlr4.Runtime.Misc;
using MergeJoinEngine.Antlr.Structs;

namespace MergeJoinEngine.Antlr
{
    internal class JoinQueryListener : MySqlParserBaseListener
    {
        public JoinQueryListener()
        {
            Result = new JoinQueryParseResult();
        }

        public JoinQueryParseResult Result { get; }

        public override void EnterSelectColumnElement([NotNull] MySqlParser.SelectColumnElementContext context)
        {
            Result.Columns.Add(SelectColumn.Create(context));
        }
        
        public override void EnterAtomTableItem(MySqlParser.AtomTableItemContext context)
        {
            Result.Tables.Add(Table.Create(context));
        }
        
        public override void EnterBinaryComparasionPredicate([NotNull] MySqlParser.BinaryComparasionPredicateContext context)
        {
            Result.Joins.Add(new Tuple<Column, Column>(Column.Create(context.children[0].GetChild(0) as MySqlParser.FullColumnNameExpressionAtomContext),
                Column.Create(context.children[2].GetChild(0) as MySqlParser.FullColumnNameExpressionAtomContext)));
        }

        public override void EnterInnerJoin(MySqlParser.InnerJoinContext context)
        {
            base.EnterInnerJoin(context);
        }

        public override void EnterOuterJoin(MySqlParser.OuterJoinContext context)
        {
            Result.IsLeftOuterJoin = true;
            base.EnterOuterJoin(context);
        }

        public override void EnterNaturalJoin(MySqlParser.NaturalJoinContext context)
        {
            base.EnterNaturalJoin(context);
        }
    }
}
