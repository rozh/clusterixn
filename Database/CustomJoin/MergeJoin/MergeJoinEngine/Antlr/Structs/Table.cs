﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

namespace MergeJoinEngine.Antlr.Structs
{
    internal class Table
    {
        public string TableName { get; set; }
        public string TableAlias { get; set; }

        public static Table Create(MySqlParser.AtomTableItemContext context)
        {
            var column = new Table();

            if (context.ChildCount > 2)
            {
                column.TableName = context.GetChild(0).GetText();
                column.TableAlias = context.GetChild(2).GetText();
            }
            else
            {
                column.TableName = context.GetText();
            }

            return column;
        }
    }
}
