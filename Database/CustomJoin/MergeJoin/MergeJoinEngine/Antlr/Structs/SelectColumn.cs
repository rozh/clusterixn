﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

namespace MergeJoinEngine.Antlr.Structs
{
    internal class SelectColumn : Column
    {
        public string ColumnAlias { get; set; }

        public static SelectColumn Create(MySqlParser.SelectColumnElementContext context)
        {
            var column = new SelectColumn();
            var child = context.children[0];
            if (child.ChildCount > 1)
            {
                column.ColumnName = child.GetChild(1).GetText().Substring(1);
                column.TableAlias = child.GetChild(0).GetText();
            }
            else
            {
                column.ColumnName = child.GetText();
            }
            if (context.ChildCount > 2)
            {
                column.ColumnAlias = context.GetChild(2).GetText();
            }

            return column;
        }
    }
}
