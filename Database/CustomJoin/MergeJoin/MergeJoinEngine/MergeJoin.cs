#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClusterixN.Common;
using ClusterixN.Common.BinaryStorage;
using ClusterixN.Common.Data.EventArgs;
using ClusterixN.Common.Data.Query.Relation;
using ClusterixN.Common.Interfaces;
using LZ4;
using MergeJoinEngine.Antlr;

namespace MergeJoinEngine
{
    public class MergeJoin
    {
        protected readonly ILogger Logger;
        private StringBuilder JoinRowBuffer;
        private ConcurrentQueue<WriteChunkQueueTask> _chunkQueue;
        private bool _useCompression;
        private string _connectionString;
        private bool _isPause;
        private bool _isStopSelect;
        private readonly object _pauseWaitSyncObject = new object();

        public MergeJoin()
        {
            Logger = ServiceLocator.Instance.LogService.GetLogger("defaultLogger");


            JoinRowBuffer = new StringBuilder(2048);
            _chunkQueue = new ConcurrentQueue<WriteChunkQueueTask>();
        }

        public MergeJoin(string connectionString) : this()
        {
            ConnectionString = connectionString;
        }


        public string ConnectionString
        {
            get => _connectionString;
            set
            {
                _connectionString = value;
                _useCompression = ConnectionStringParser.GetUseCompression(ConnectionString);
            }
        }

        private bool IsPause
        {
            get
            {
                lock (_pauseWaitSyncObject)
                {
                    return _isPause;
                }
            }
            set
            {
                lock (_pauseWaitSyncObject)
                {
                    _isPause = value;
                }
            }
        }
        protected bool IsStopSelect
        {
            get
            {
                lock (_pauseWaitSyncObject)
                {
                    return _isStopSelect;
                }
            }
            set
            {
                lock (_pauseWaitSyncObject)
                {
                    _isStopSelect = value;
                }
            }
        }

        /// <summary>
        /// True если запрос остановлен
        /// </summary>
        /// <returns></returns>
        protected bool WaitPauseOrStop()
        {
            while (IsPause && !IsStopSelect)
            {
                Thread.Sleep(500);
            }
            
            if (IsStopSelect)
            {
                IsStopSelect = false;
                return true;
            }

            return false;
        }

        public byte[] ResultToBinaryFormat(RelationSchema schema, byte[] data)
        {
            var tableMeta = new FileMeta()
            {
                Columns = schema.Fields.Select(x => x.Name).ToArray(),
                Types = schema.Fields.Select(x => StringTypeToColumnType(x.Params)).ToArray()
            };

            var indexes = new List<ColumnIndex>(schema.Indexes.Count);
            foreach (var field in schema.Indexes.SelectMany(x => x.FieldNames))
            {
                if (tableMeta.Columns.All(x => x.ToLower() != field.ToLower())) continue;

                for (var i = 0; i < tableMeta.Columns.Length; i++)
                {
                    if (tableMeta.Columns[i].ToLower() == field.ToLower())
                    {
                        indexes.Add(new ColumnIndex(i, false));
                        break;
                    }
                }
            }
            tableMeta.Indexes = indexes.ToArray();

            using (var output = new MemoryStream())
            {
                using (var input = new MemoryStream(data))
                {
                    using (var inputReader = new StreamReader(input, Encoding.UTF8, true))
                    {
                        using (var tmpWriter = new TableBinaryWriter(output, tableMeta))
                        {
                            string line;
                            while ((line = inputReader.ReadLine()) != null)
                            {
                                if (string.IsNullOrWhiteSpace(line)) continue;
                                tmpWriter.WriteTableRow(ParseLine(line, ref tableMeta));
                            }
                        }
                    }
                }

                return output.ToArray();
            }
        }

        public void LoadFileBinaryFile(string filePath, string tableName)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            var tmpTable = tableName + "_sort_";
            var chunkSize = ConnectionStringParser.GetSortChunkSize(ConnectionString);
            var chunkNumber = 0;
            var chunkedFiles = new List<string>();

            using (var reader = new TableBinaryReader(tableName, dir, _useCompression))
            {
                using (var input = new TableBinaryReader(filePath, reader.Meta, _useCompression))
                {
                    var chunkTask = new WriteChunkQueueTask(chunkSize, reader);
                    var rowIndex = 0;
                    var cancellationTokenSource = new CancellationTokenSource();
                    var writeTask = new Task(SortAndWriteConsumer, cancellationTokenSource.Token);
                    writeTask.Start();

                    TableRow? line;
                    while ((line = input.ReadNextTableRow()) != null)
                    {
                        chunkTask.AddRow(line.Value);
                        rowIndex++;
                        if (rowIndex == chunkSize)
                        {
                            var chunkName = tmpTable + chunkNumber;
                            chunkedFiles.Add(chunkName);
                            chunkNumber++;
                            chunkTask.ChunkName = chunkName;
                            chunkTask.Dir = dir;

                            _chunkQueue.Enqueue(chunkTask);

                            chunkTask = new WriteChunkQueueTask(chunkSize, reader);
                            rowIndex = 0;
                        }
                    }

                    if (rowIndex > 0)
                    {
                        var chunkName = tmpTable + chunkNumber;
                        chunkedFiles.Add(chunkName);
                        chunkTask.ChunkName = chunkName;
                        chunkTask.Dir = dir;
                        
                        _chunkQueue.Enqueue(chunkTask);
                    }

                    cancellationTokenSource.Cancel();
                    writeTask.Wait();
                }

            }

            if (!chunkedFiles.Any()) return; //пустая таблица

            DropTable(tableName);

            MergeSortTables(tableName, chunkedFiles, tmpTable);
        }

        public void LoadFile(string filePath, string tableName)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            var tmpTable = tableName + "_sort_";
            var chunkSize = ConnectionStringParser.GetSortChunkSize(ConnectionString);
            var chunkNumber = 0;
            var chunkedFiles = new List<string>();

            using (var input = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 1024 * 1024))
            {
                using (var inputReader = new StreamReader(input, Encoding.UTF8, true, 1024 * 128))
                {
                    using (var reader = new TableBinaryReader(tableName, dir, _useCompression))
                    {
                        var chunkTask = new WriteChunkQueueTask(chunkSize, reader);
                        var rowIndex = 0;
                        var cancellationTokenSource = new CancellationTokenSource();
                        var writeTask = new Task(SortAndWriteConsumer, cancellationTokenSource.Token);
                        writeTask.Start();

                        string line;
                        while ((line = inputReader.ReadLine()) != null)
                        {
                            if (string.IsNullOrWhiteSpace(line)) continue;
                            chunkTask.AddRow(ParseLine(line, ref reader.Meta));
                            rowIndex++;
                            if (rowIndex == chunkSize)
                            {
                                var chunkName = tmpTable + chunkNumber;
                                chunkedFiles.Add(chunkName);
                                chunkNumber++;
                                chunkTask.ChunkName = chunkName;
                                chunkTask.Dir = dir;

                                //_chunkQueue.Enqueue(chunkTask);

                                SortAndWriteChunk(chunkTask);
                                //chunkTask.Chunk.Clear();

                                chunkTask = new WriteChunkQueueTask(chunkSize, reader);
                                rowIndex = 0;
                            }
                        }

                        if (rowIndex > 0)
                        {
                            var chunkName = tmpTable + chunkNumber;
                            chunkedFiles.Add(chunkName);
                            chunkTask.ChunkName = chunkName;
                            chunkTask.Dir = dir;

                            SortAndWriteChunk(chunkTask);
                            //chunkTask.Chunk.Clear();
                            //_chunkQueue.Enqueue(chunkTask);
                        }

                        cancellationTokenSource.Cancel();
                        writeTask.Wait();
                    }
                }
            }

            if (!chunkedFiles.Any()) return; //пустая таблица

            DropTable(tableName);

            MergeSortTables(tableName, chunkedFiles, tmpTable);
        }

        private void SortAndWriteConsumer(object obj)
        {
            var token = (CancellationToken)obj;
            while (!token.IsCancellationRequested || !_chunkQueue.IsEmpty)
            {
                if (_chunkQueue.TryDequeue(out var task))
                {
                    SortAndWriteChunk(task);
                    task.Clear();
                }
                else
                {
                    token.WaitHandle.WaitOne(100);
                }
            }
        }

        private void SortAndWriteChunk(WriteChunkQueueTask task)
        {
            var sortedChunk = task.ChunkPointers.OrderBy(x => x, new ChunkPointerComparer());
            var outputFile = string.Empty;
            using (var tmpWriter = new TableBinaryWriter(task.ChunkName.ToUpper(), task.Dir, _useCompression))
            {
                tmpWriter.SaveMeta(task.Reader.Meta);
                outputFile = tmpWriter.FileName;
            }
            var data = task.GetChunkData();
            using (var fileStream = new FileStream(outputFile, FileMode.OpenOrCreate, FileAccess.Write,
                       FileShare.None, 1024 * 1024, FileOptions.SequentialScan))
            {
                if (_useCompression)
                {

                    using (var lz4Stream = new LZ4Stream(fileStream, LZ4StreamMode.Compress))
                    {
                        foreach (var row in sortedChunk)
                        {
                            lz4Stream.Write(data, row.Offset, row.Length);
                        }
                    }
                }
                else
                {
                    foreach (var row in sortedChunk)
                    {
                        fileStream.Write(data, row.Offset, row.Length);
                    }
                }
            }

        }

        private void MergeSortTables(string tableName, List<string> chunkedFiles, string tmpTableName)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);

            var mergedFiles = new List<string>();
            var iteration = 0;

            while (chunkedFiles.Count > 1)
            {
                iteration++;
                for (var i = 0; i < chunkedFiles.Count - 1; i += 2)
                {
                    var mergeFileName = $"{tmpTableName}i_{iteration}_{i}";
                    mergedFiles.Add(mergeFileName);
                    MergeSortTable(chunkedFiles[i], chunkedFiles[i + 1], mergeFileName);
                }

                if (chunkedFiles.Count % 2 != 0)
                {
                    mergedFiles.Add(chunkedFiles.Last());
                }
                chunkedFiles = mergedFiles.ToList();
                mergedFiles.Clear();
            }

            using (var w = new TableBinaryWriter(chunkedFiles[0].ToUpper(), dir, _useCompression))
            {
                w.Rename(tableName.ToUpper());
                w.Dispose();
            }
        }

        private void MergeSortTable(string leftTable, string rightTable, string resultTable)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            using (var leftReader = new TableBinaryReader(leftTable.ToUpper(), dir, _useCompression))
            {
                using (var rightReader = new TableBinaryReader(rightTable.ToUpper(), dir, _useCompression))
                {
                    var comparer = new TableRowComparer(leftReader.Meta, rightReader.Meta);
                    var blockSize = ConnectionStringParser.GetSortChunkSize(ConnectionString);
                    long count = 0;
                    using (var resultWriter = new TableBinaryWriter(resultTable.ToUpper(), dir, _useCompression))
                    {
                        resultWriter.SaveMeta(leftReader.Meta);

                        var leftRow = leftReader.ReadNextTableRow();
                        var rightRow = rightReader.ReadNextTableRow();

                        while (leftRow != null && rightRow != null)
                        {
                            count++;
                            var compareResult = comparer.Compare(leftRow, rightRow);
                            if (compareResult == 0)
                            {

                                resultWriter.WriteTableRow(leftRow.Value);
                                resultWriter.WriteTableRow(rightRow.Value);

                                leftRow = leftReader.ReadNextTableRow();
                                rightRow = rightReader.ReadNextTableRow();
                                count++;
                            }
                            else if (compareResult > 0)
                            {
                                resultWriter.WriteTableRow(rightRow.Value);
                                rightRow = rightReader.ReadNextTableRow();
                            }
                            else
                            {
                                resultWriter.WriteTableRow(leftRow.Value);
                                leftRow = leftReader.ReadNextTableRow();
                            }

                            if (count % blockSize == 0)
                                resultWriter.FlushData();
                        }

                        foreach (var lastLeftRow in leftReader.ReadTableRow())
                        {
                            resultWriter.WriteTableRow(lastLeftRow.Value);
                            count++;
                            if (count % blockSize == 0)
                                resultWriter.FlushData();
                        }

                        foreach (var lastRightRow in rightReader.ReadTableRow())
                        {
                            resultWriter.WriteTableRow(lastRightRow.Value);
                            count++;
                            if (count % blockSize == 0)
                                resultWriter.FlushData();
                        }
                    }
                }
            }

            DropTable(leftTable);
            DropTable(rightTable);
        }

        private static IEnumerable<string> SplitString(string line)
        {
            var startIndex = 0;
            var offsetLeft = 0;
            var offsetRight = 0;
            if (line[0] == '"')
            {
                offsetLeft = 1;
                offsetRight = 2;
            }

            for (var endIndex = 0; endIndex < line.Length; endIndex++)
            {
                if (line[endIndex] == '|')
                {
                    yield return line.Substring(startIndex + offsetLeft, endIndex - startIndex - offsetRight);
                    startIndex = endIndex + 1;
                }
            }

            if (startIndex + offsetLeft <= line.Length - offsetRight)
            {
                yield return line.Substring(startIndex + offsetLeft, line.Length - startIndex - offsetRight);
            }
        }

        private static TableRow ParseLine(string line, ref FileMeta meta)
        {
            var types = meta.Types;
            var culture = CultureInfo.InvariantCulture;
            var row = new TableRow(ref meta);
            var i = 0;
            foreach (var part in SplitString(line))
            {
                if (i == types.Length) break;

                switch (types[i])
                {
                    case ColumnType.LONG:
                        row.AddValue(Convert.ToInt64(part, culture), i);
                        break;
                    case ColumnType.INT:
                        row.AddValue(Convert.ToInt32(part, culture), i);
                        break;
                    case ColumnType.FLOAT:
                        row.AddValue(Convert.ToSingle(part, culture), i);
                        break;
                    case ColumnType.DOUBLE:
                        row.AddValue(Convert.ToDouble(part, culture), i);
                        break;
                    case ColumnType.BOOLEAN:
                        row.AddValue(Convert.ToBoolean(part), i);
                        break;
                    case ColumnType.STRING:
                        row.AddValue(part, i);
                        break;
                    case ColumnType.DATE:
                        var dp = part.Split('-');

                        var dt = new DateTime(int.Parse(dp[0], 0, culture),
                            int.Parse(dp[1], 0, culture),
                            int.Parse(dp[2], 0, culture));
                        row.AddValue(dt, i);

                        break;
                    case ColumnType.TIME:
                        row.AddValue(DateTime.ParseExact(part, "HH:mm:ss", culture), i);
                        break;
                    case ColumnType.DATETIME:
                        row.AddValue(Convert.ToDateTime(part, culture), i);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                i++;
            }

            return row;

        }

        public void DropTable(string tableName)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            using (var writer = new TableBinaryWriter(tableName.ToUpper(), dir, _useCompression))
            {
                writer.DropTable();
            }
        }

        public void DropTmpRealtions()
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            foreach (var d in Directory.EnumerateDirectories(dir))
            {
                Directory.Delete(d, true);
            }
            foreach (var file in Directory.EnumerateFiles(dir, "*_tmp*"))
            {
                File.Delete(file);
            }
        }

        public void CreateRelation(string name, string[] fields, string[] types)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            using (var writer = new TableBinaryWriter(name.ToUpper(), dir, _useCompression))
            {
                var meta = new FileMeta()
                {
                    Columns = fields,
                    Types = types.Select(StringTypeToColumnType).ToArray()
                };

                writer.SaveMeta(meta);
            }
        }

        private ColumnType StringTypeToColumnType(string type)
        {
            var strType = type.ToLower();

            if (strType.Contains("long") || strType.Contains("bigint"))
            {
                return ColumnType.LONG;
            }

            if (strType.Contains("int"))
            {
                return ColumnType.INT;
            }

            if (strType.Contains("bool"))
            {
                return ColumnType.BOOLEAN;
            }

            if (strType.Contains("decimal") || strType.Contains("float"))
            {
                return ColumnType.FLOAT;
            }

            if (strType.Contains("time"))
            {
                return ColumnType.TIME;
            }
            if (strType.Contains("date"))
            {
                return ColumnType.DATE;
            }
            if (strType.Contains("datetime"))
            {
                return ColumnType.DATETIME;
            }

            return ColumnType.STRING;
        }

        public void AddIndex(string name, string relation, string[] fields)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            using (var writer = new TableBinaryWriter(relation.ToUpper(), dir, _useCompression))
            {
                AddIndexes(fields, writer, false);
            }
        }

        private static void AddIndexes(string[] fields, TableBinaryWriter writer, bool isPrimary)
        {
            var indexes = new List<ColumnIndex>(writer.Meta.Indexes);
            foreach (var field in fields)
            {
                if (writer.Meta.Columns.Any(x => x.ToLower() == field.ToLower()))
                {
                    for (var i = 0; i < writer.Meta.Columns.Length; i++)
                    {
                        if (writer.Meta.Columns[i].ToLower() == field.ToLower())
                        {
                            indexes.Add(new ColumnIndex(i, isPrimary));
                            break;
                        }
                    }
                }
            }

            writer.Meta.Indexes = indexes.ToArray();
            writer.SaveMeta(writer.Meta);
        }

        public void AddPrimaryKey(string relation, string[] fields)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            using (var writer = new TableBinaryWriter(relation.ToUpper(), dir, _useCompression))
            {
                AddIndexes(fields, writer, true);
            }
        }

        /// <summary>
        ///     Выборка по блокам из БД
        /// </summary>
        /// <param name="query">Запрос к БД</param>
        /// <param name="blockSize">Размер блока в строках</param>
        public void SelectBlocks(string query, int blockSize)
        {
            var parser = new JoinQueryParser();
            var parseResult = parser.Parse(query);

            Join(parseResult, blockSize);
        }

        private void Join(JoinQueryParseResult joinQueryParseResult, int blockSize)
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            var chunk = new StringBuilder();
            var leftTable = joinQueryParseResult.Tables.First().TableName;
            var rightTable = joinQueryParseResult.Tables.Last().TableName;

            var sendcount = 0;

            using (var leftReader = new TableBinaryReader(leftTable.ToUpper(), dir, _useCompression))
            {
                using (var rightReader = new TableBinaryReader(rightTable.ToUpper(), dir, _useCompression))
                {
                    var leftTableColumns = new List<string>();
                    var rightTableColumns = new List<string>();
                    var columnOrder = new List<int>();

                    if (joinQueryParseResult.IsLeftOuterJoin)
                    {
                        PrepareJoin(joinQueryParseResult, leftReader, leftTableColumns, columnOrder, rightReader, rightTableColumns);
                        LeftOuterJoin(blockSize, columnOrder, leftReader, leftTableColumns, rightReader, rightTableColumns, chunk, ref sendcount);
                    }
                    else if (leftReader.Meta.Indexes.All(x => !x.IsPrimary) &&
                             rightReader.Meta.Indexes.All(x => !x.IsPrimary))
                    {
                        //нет уникальных ключей
                        PrepareJoin(joinQueryParseResult, leftReader, leftTableColumns, columnOrder, rightReader, rightTableColumns);
                        CrossMergeJoin(blockSize, columnOrder, leftReader, leftTableColumns, rightReader, rightTableColumns, chunk, ref sendcount);
                    }
                    else
                    {
                        if (leftReader.Meta.Indexes.Any(x => x.IsPrimary) &&
                            rightReader.Meta.Indexes.Any(x => x.IsPrimary))
                        {
                            //уникальный ключ у обоих таблиц. упрощаем задачу
                            PrepareJoin(joinQueryParseResult, leftReader, leftTableColumns, columnOrder, rightReader, rightTableColumns);
                            InnerMergeJoin(blockSize, columnOrder, leftReader, leftTableColumns, rightReader, rightTableColumns, chunk, ref sendcount);
                        }
                        else
                        {
                            if (!leftReader.Meta.Indexes.Any(x => x.IsPrimary))
                            {
                                //уникальный ключ у правой таблицы. меняем таблицы местами
                                var primaryTableReader = rightReader;
                                var secondaryTableReader = leftReader;

                                PrepareJoin(joinQueryParseResult, primaryTableReader, leftTableColumns, columnOrder, secondaryTableReader, rightTableColumns);
                                LeftJoin(blockSize, columnOrder, primaryTableReader, leftTableColumns, secondaryTableReader, rightTableColumns, chunk, ref sendcount);
                            }
                            else
                            {
                                //уникальный ключ у левой таблицы
                                PrepareJoin(joinQueryParseResult, leftReader, leftTableColumns, columnOrder, rightReader, rightTableColumns);
                                LeftJoin(blockSize, columnOrder, leftReader, leftTableColumns, rightReader, rightTableColumns, chunk, ref sendcount);
                            }

                        }
                    }
                }
            }

            //отправка последнего блока
            OnBlockReaded(chunk.Length > 0 ? Encoding.UTF8.GetBytes(chunk.ToString()) : new byte[0], true, sendcount);
        }
        private void CrossMergeJoin(int blockSize, List<int> columnOrder, TableBinaryReader leftReader, List<string> leftTableColumns,
            TableBinaryReader rightReader, List<string> rightTableColumns, StringBuilder chunk, ref int sendcount)
        {
            uint count = 0;
            var columnOrderArr = columnOrder.ToArray();

            var leftTableColumnsInd = GetColumnsInd(leftReader.Meta.Columns, leftTableColumns.ToArray());
            var rightTableColumnsInd = GetColumnsInd(rightReader.Meta.Columns, rightTableColumns.ToArray());

            var leftRow = leftReader.ReadNextTableRow();
            var rightRow = rightReader.ReadNextTableRow();
            var comparer = new TableRowComparer(leftReader.Meta, rightReader.Meta);
            var leftComparer = new TableRowComparer(leftReader.Meta, leftReader.Meta);
            var rightComparer = new TableRowComparer(rightReader.Meta, rightReader.Meta);
            var leftTypes = leftReader.Meta.Types;
            var rightTypes = rightReader.Meta.Types;
            var doubleLeftRows = new List<TableRow>();
            var doubleRightRows = new List<TableRow>();
            TableRow? prevRightRow = null;
            TableRow? prevLeftRow = null;
            var lastRow = string.Empty;

            while (leftRow != null || rightRow != null)
            {
                if (!string.IsNullOrEmpty(lastRow) && rightRow != null && prevRightRow != null && rightComparer.Compare(rightRow, prevRightRow) == 0)
                {
                    if (doubleRightRows.Count == 0)
                        doubleRightRows.Add(prevRightRow.Value);
                    doubleRightRows.Add(rightRow.Value);


                    prevRightRow = rightRow;
                    rightRow = rightReader.ReadNextTableRow();

                    continue;
                }

                if (!string.IsNullOrEmpty(lastRow) && leftRow != null && prevLeftRow != null && leftComparer.Compare(leftRow, prevLeftRow) == 0)
                {
                    if (doubleLeftRows.Count == 0)
                        doubleLeftRows.Add(prevLeftRow.Value);
                    doubleLeftRows.Add(leftRow.Value);

                    prevLeftRow = leftRow;
                    leftRow = leftReader.ReadNextTableRow();

                    continue;
                }

                lastRow = SendCrossJoinResult(blockSize, chunk, ref sendcount, doubleLeftRows, doubleRightRows, lastRow,
                    leftTableColumnsInd, rightTableColumnsInd, columnOrderArr, prevRightRow, prevLeftRow, ref count, leftTypes, rightTypes);

                var compareResult = rightRow != null ? (leftRow != null ? comparer.Compare(leftRow, rightRow) : 1) : -1;
                if (compareResult == 0)
                {
                    lastRow = JoinRows(leftRow.Value, rightRow.Value, leftTableColumnsInd, rightTableColumnsInd,
                        columnOrderArr, leftTypes, rightTypes);

                    prevRightRow = rightRow;
                    rightRow = rightReader.ReadNextTableRow();
                    prevLeftRow = leftRow;
                    leftRow = leftReader.ReadNextTableRow();
                }
                else if (compareResult > 0)
                {
                    prevRightRow = rightRow;
                    rightRow = rightReader.ReadNextTableRow();
                }
                else
                {
                    prevLeftRow = leftRow;
                    leftRow = leftReader.ReadNextTableRow();
                }
            }

            SendCrossJoinResult(blockSize, chunk, ref sendcount, doubleLeftRows, doubleRightRows, lastRow,
                leftTableColumnsInd, rightTableColumnsInd, columnOrderArr, prevRightRow, prevLeftRow, ref count, leftTypes, rightTypes);

        }

        private string SendCrossJoinResult(int blockSize, StringBuilder chunk, ref int sendcount, List<TableRow> doubleLeftRows,
            List<TableRow> doubleRightRows, string lastRow, int[] leftTableColumnsInd, int[] rightTableColumnsInd, int[] columnOrderArr,
            TableRow? prevRightRow, TableRow? prevLeftRow, ref uint count, ColumnType[] leftRowTypes, ColumnType[] rightRowTypes)
        {
            if (doubleLeftRows.Count > 0 || doubleRightRows.Count > 0)
            {
                lastRow = string.Empty;
                if (doubleLeftRows.Count > 0 && doubleRightRows.Count > 0)
                {
                    foreach (var doubleLeftRow in doubleLeftRows)
                    {
                        foreach (var doubleRightRow in doubleRightRows)
                        {
                            chunk.Append(JoinRows(doubleLeftRow, doubleRightRow, leftTableColumnsInd,
                                rightTableColumnsInd,
                                columnOrderArr, leftRowTypes, rightRowTypes));
                            count++;
                            if (!SendJoinResultBuffer(blockSize, chunk, ref sendcount, count)) return lastRow;
                        }
                    }

                    doubleLeftRows.Clear();
                    doubleRightRows.Clear();
                }

                if (doubleRightRows.Count == 0 && prevRightRow != null)
                {
                    foreach (var doubleLeftRow in doubleLeftRows)
                    {
                        chunk.Append(JoinRows(doubleLeftRow, prevRightRow.Value, leftTableColumnsInd, rightTableColumnsInd,
                            columnOrderArr, leftRowTypes, rightRowTypes));
                        count++;
                        if (!SendJoinResultBuffer(blockSize, chunk, ref sendcount, count)) return lastRow;
                    }

                    doubleLeftRows.Clear();
                }

                if (doubleLeftRows.Count == 0 && prevLeftRow != null)
                {
                    foreach (var doubleRightRow in doubleRightRows)
                    {
                        chunk.Append(JoinRows(prevLeftRow.Value, doubleRightRow, leftTableColumnsInd, rightTableColumnsInd,
                            columnOrderArr, leftRowTypes, rightRowTypes));
                        count++;
                        if (!SendJoinResultBuffer(blockSize, chunk, ref sendcount, count)) return lastRow;
                    }

                    doubleRightRows.Clear();
                }
            }


            if (!string.IsNullOrEmpty(lastRow))
            {
                chunk.Append(lastRow);
                count++;
                SendJoinResultBuffer(blockSize, chunk, ref sendcount, count);
                lastRow = string.Empty;
            }

            return lastRow;
        }

        private void InnerMergeJoin(int blockSize, List<int> columnOrder, TableBinaryReader leftReader, List<string> leftTableColumns,
            TableBinaryReader rightReader, List<string> rightTableColumns, StringBuilder chunk, ref int sendcount)
        {
            uint count = 0;
            var columnOrderArr = columnOrder.ToArray();
            var comparer = new TableRowComparer(leftReader.Meta, rightReader.Meta);
            var leftTypes = leftReader.Meta.Types;
            var rightTypes = rightReader.Meta.Types;

            var leftTableColumnsInd = GetColumnsInd(leftReader.Meta.Columns, leftTableColumns.ToArray());
            var rightTableColumnsInd = GetColumnsInd(rightReader.Meta.Columns, rightTableColumns.ToArray());

            var leftRow = leftReader.ReadNextTableRow();
            var rightRow = rightReader.ReadNextTableRow();

            while (leftRow != null && rightRow != null)
            {
                var compareResult = comparer.Compare(leftRow, rightRow);
                if (compareResult == 0)
                {
                    chunk.Append(JoinRows(leftRow.Value, rightRow.Value, leftTableColumnsInd, rightTableColumnsInd,
                        columnOrderArr, leftTypes, rightTypes));
                    count++;

                    if (!SendJoinResultBuffer(blockSize, chunk, ref sendcount, count)) return;

                    rightRow = rightReader.ReadNextTableRow();
                    leftRow = leftReader.ReadNextTableRow();
                }
                else if (compareResult > 0)
                {
                    rightRow = rightReader.ReadNextTableRow();
                }
                else
                {

                    leftRow = leftReader.ReadNextTableRow();
                }
            }
        }

        private void LeftJoin(int blockSize, List<int> columnOrder, TableBinaryReader leftReader, List<string> leftTableColumns,
            TableBinaryReader rightReader, List<string> rightTableColumns, StringBuilder chunk, ref int sendcount)
        {
            uint count = 0;
            var columnOrderArr = columnOrder.ToArray();
            var comparer = new TableRowComparer(leftReader.Meta, rightReader.Meta);
            var leftTypes = leftReader.Meta.Types;
            var rightTypes = rightReader.Meta.Types;

            var leftTableColumnsInd = GetColumnsInd(leftReader.Meta.Columns, leftTableColumns.ToArray());
            var rightTableColumnsInd = GetColumnsInd(rightReader.Meta.Columns, rightTableColumns.ToArray());

            var leftRow = leftReader.ReadNextTableRow();
            var rightRow = rightReader.ReadNextTableRow();

            while (leftRow != null && rightRow != null)
            {
                var compareResult = comparer.Compare(leftRow, rightRow);
                if (compareResult == 0)
                {
                    chunk.Append(JoinRows(leftRow.Value, rightRow.Value, leftTableColumnsInd, rightTableColumnsInd,
                        columnOrderArr, leftTypes, rightTypes));
                    count++;

                    if (!SendJoinResultBuffer(blockSize, chunk, ref sendcount, count)) return;

                    rightRow = rightReader.ReadNextTableRow();
                }
                else if (compareResult > 0)
                {
                    rightRow = rightReader.ReadNextTableRow();
                }
                else
                {

                    leftRow = leftReader.ReadNextTableRow();
                }
            }
        }

        private void LeftOuterJoin(int blockSize, List<int> columnOrder, TableBinaryReader leftReader, List<string> leftTableColumns,
            TableBinaryReader rightReader, List<string> rightTableColumns, StringBuilder chunk, ref int sendcount)
        {

            uint count = 0;
            var columnOrderArr = columnOrder.ToArray();
            var comparer = new TableRowComparer(leftReader.Meta, rightReader.Meta);
            var leftTypes = leftReader.Meta.Types;
            var rightTypes = rightReader.Meta.Types;

            var leftTableColumnsInd = GetColumnsInd(leftReader.Meta.Columns, leftTableColumns.ToArray());
            var rightTableColumnsInd = GetColumnsInd(rightReader.Meta.Columns, rightTableColumns.ToArray());

            var leftRow = leftReader.ReadNextTableRow();
            var rightRow = rightReader.ReadNextTableRow();

            var emptyRightRow = new TableRow(ref rightReader.Meta);
            var leftHasJoin = false;
            while (leftRow != null || rightRow != null)
            {
                var compareResult = rightRow != null ? leftRow == null ? 1 : comparer.Compare(leftRow, rightRow) : -1;

                if (compareResult == 0)
                {
                    chunk.Append(JoinRows(leftRow.Value, rightRow.Value, leftTableColumnsInd, rightTableColumnsInd,
                        columnOrderArr, leftTypes, rightTypes));
                    count++;
                    SendJoinResultBuffer(blockSize, chunk, ref sendcount, count);
                    leftHasJoin = true;

                    rightRow = rightReader.ReadNextTableRow();
                }
                else if (compareResult > 0)
                {
                    rightRow = rightReader.ReadNextTableRow();
                }
                else
                {
                    if (!leftHasJoin)
                    {
                        chunk.Append(JoinRows(leftRow.Value, emptyRightRow, leftTableColumnsInd, rightTableColumnsInd,
                            columnOrderArr, leftTypes, rightTypes));
                        count++;
                        SendJoinResultBuffer(blockSize, chunk, ref sendcount, count);
                    }

                    leftRow = leftReader.ReadNextTableRow();
                    leftHasJoin = false;
                }
            }
        }

        private bool SendJoinResultBuffer(int blockSize, StringBuilder chunk, ref int sendcount, uint count)
        {
            if (count % blockSize == 0) //отправка блока
            {
                var dest = new char[chunk.Length];
                chunk.CopyTo(0, dest, 0, chunk.Length);
                var sendBuffer = Encoding.UTF8.GetBytes(dest);
                chunk.Clear();
                OnBlockReaded(sendBuffer, orderNumber: sendcount++);

                return !WaitPauseOrStop();
            }

            return true;
        }

        private static void PrepareJoin(JoinQueryParseResult joinQueryParseResult, TableBinaryReader leftReader,
            List<string> leftTableColumns, List<int> columnOrder, TableBinaryReader rightReader, List<string> rightTableColumns)
        {
            for (var i = 0; i < joinQueryParseResult.Columns.Count; i++)
            {
                string table = null;
                if (!string.IsNullOrWhiteSpace(joinQueryParseResult.Columns[i].TableAlias))
                {
                    table = joinQueryParseResult.Tables.FirstOrDefault(x =>
                        x.TableAlias == joinQueryParseResult.Columns[i].TableAlias)?.TableName;
                }

                if (table == leftReader.Table)
                {
                    foreach (var column in leftReader.Meta.Columns)
                    {
                        if (joinQueryParseResult.Columns[i].ColumnName == column)
                        {
                            leftTableColumns.Add(column);
                            break;
                        }
                    }
                }
                else if (table == rightReader.Table)
                {
                    foreach (var column in rightReader.Meta.Columns)
                    {
                        if (joinQueryParseResult.Columns[i].ColumnName == column)
                        {
                            rightTableColumns.Add(column);
                            break;
                        }
                    }
                }
                else
                {
                    var found = false;
                    foreach (var column in leftReader.Meta.Columns)
                    {
                        if (joinQueryParseResult.Columns[i].ColumnName == column)
                        {
                            leftTableColumns.Add(column);
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        foreach (var column in rightReader.Meta.Columns)
                        {
                            if (joinQueryParseResult.Columns[i].ColumnName == column)
                            {
                                rightTableColumns.Add(column);
                                break;
                            }
                        }
                    }
                }
            }

            for (var i = 0; i < joinQueryParseResult.Columns.Count; i++)
            {
                var column = joinQueryParseResult.Columns[i].ColumnName;
                var found = false;
                for (var j = 0; j < leftTableColumns.Count; j++)
                {
                    if (column == leftTableColumns[j])
                    {
                        columnOrder.Add(j);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    for (var j = 0; j < rightTableColumns.Count; j++)
                    {
                        if (column == rightTableColumns[j])
                        {
                            columnOrder.Add(j + leftTableColumns.Count);
                            break;
                        }
                    }
                }
            }
        }

        private int[] GetColumnsInd(string[] tableColumns, string[] resultColumns)
        {
            var columnsIndList = new List<int>(resultColumns.Length);
            for (var i = 0; i < resultColumns.Length; i++)
            {
                var column = resultColumns[i];
                for (var j = 0; j < tableColumns.Length; j++)
                {
                    if (tableColumns[j].ToUpper() == column.ToUpper())
                    {
                        columnsIndList.Add(j);
                        break;
                    }
                }
            }
            return columnsIndList.ToArray();
        }

        private string JoinRows(TableRow leftRow, TableRow rightRow, int[] leftTableColumns, int[] rightTableColumns,
            int[] columnOrder, ColumnType[] leftRowTypes, ColumnType[] rightRowTypes)
        {
            var first = true;
            object value;
            ColumnType columnType;
            var index = 0;

            foreach (var i in columnOrder)
            {
                if (!first)
                {
                    JoinRowBuffer.Append('|');
                }
                else
                {
                    first = false;
                }

                if (i < leftTableColumns.Length)
                {
                    index = leftTableColumns[i];
                    value = leftRow[index];
                    columnType = leftRowTypes[index];
                }
                else
                {
                    index = rightTableColumns[i - leftTableColumns.Length];
                    value = rightRow[index];
                    columnType = rightRowTypes[index];
                }
                JoinRowBuffer.Append(value == null ? ColumnToString(value, columnType) : $"\"{ColumnToString(value, columnType)}\"");
            }

            JoinRowBuffer.Append('\n');

            var str = JoinRowBuffer.ToString();
            JoinRowBuffer.Clear();
            return str;
        }

        private string ColumnToString(object column, ColumnType columnType)
        {
            if (column == null) return "NULL";

            switch (columnType)
            {
                case ColumnType.LONG:
                    return ((long)column).ToString(CultureInfo.InvariantCulture);
                case ColumnType.INT:
                    return ((int)column).ToString(CultureInfo.InvariantCulture);
                case ColumnType.FLOAT:
                    return ((float)column).ToString(CultureInfo.InvariantCulture);
                case ColumnType.DOUBLE:
                    return ((double)column).ToString(CultureInfo.InvariantCulture);
                case ColumnType.BOOLEAN:
                    return ((bool)column).ToString(CultureInfo.InvariantCulture);
                case ColumnType.STRING:
                    return (string)column;
                case ColumnType.DATE:
                    return ((DateTime)column).ToString("yyyy-MM-dd");
                case ColumnType.TIME:
                    return ((DateTime)column).ToString("T");
                case ColumnType.DATETIME:
                    return ((DateTime)column).ToString("yyyy-MM-dd HH:mm:ss");
                default:
                    throw new ArgumentOutOfRangeException(nameof(columnType), columnType, null);
            }
        }


        public void StopSelectQuery()
        {
            IsStopSelect = true;
        }

        public void ControlSelectBlocks(bool pause)
        {
            IsPause = pause;
        }

        #region Events

        public event EventHandler<SelectResultEventArg> BlockReaded;

        protected void OnBlockReaded(byte[] rows, bool isLast = false, int orderNumber = 0)
        {
            BlockReaded?.Invoke(this, new SelectResultEventArg { Result = rows, IsLast = isLast, OrderNumber = orderNumber });
        }

        #endregion

        public long GetRelationSize(string relationName)
        {
            try
            {
                var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
                using (var reader = new TableBinaryReader(relationName.ToUpper(), dir, _useCompression))
                {
                    return reader.GetSize();
                }
            }
            catch (Exception)
            {
                //Logger.Error(e);
            }

            return 0;
        }
    }
}