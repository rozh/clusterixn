﻿using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClusterixN.Database.CustomJoin.Test
{
    [TestClass]
    public class LoadTest
    {

        private Database _db;
        private TestContext _testContextInstance;

        /// <summary>
        ///  Gets or sets the test context which provides
        ///  information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return _testContextInstance; }
            set { _testContextInstance = value; }
        }

        [TestInitialize]
        public void TestInit()
        {
            _db = new Database();
            _db.ConnectionString = @"WorkingDirectory=C:\Users\roman\source\repos\clusterixn\ClusterixN.Database.CustomJoin.Test\bin\data\db;SortChunkSize=1000000;";
        }

        [TestMethod]
        public void Load()
        {
            _db.DropTmpRealtions();
            _db.CreateRelation("lineitem", new []{ "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY", "L_LINENUMBER", "L_QUANTITY", "L_EXTENDEDPRICE", "L_DISCOUNT", "L_TAX", "L_RETURNFLAG", "L_LINESTATUS", "L_SHIPDATE", "L_COMMITDATE" , "L_RECEIPTDATE", "L_SHIPINSTRUCT", "L_SHIPMODE", "L_COMMENT" }, 
                                           new []{ "long",       "long",      "long",      "int",          "float",      "float",           "float",      "float", "string",       "string",       "date",       "date",          "date",          "string",         "string",     "string" });
            _db.AddPrimaryKey("lineitem", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY" });
            _db.LoadFile(@"D:\Clusterix\tpch\1G\tbl\lineitem.tbl", "lineitem");
        }

        [TestMethod]
        public void LoadEmpty()
        {
            _db.DropTmpRealtions();
            _db.CreateRelation("lineitem", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY", "L_LINENUMBER", "L_QUANTITY", "L_EXTENDEDPRICE", "L_DISCOUNT", "L_TAX", "L_RETURNFLAG", "L_LINESTATUS", "L_SHIPDATE", "L_COMMITDATE", "L_RECEIPTDATE", "L_SHIPINSTRUCT", "L_SHIPMODE", "L_COMMENT" },
                new[] { "long", "long", "long", "int", "float", "float", "float", "float", "string", "string", "date", "date", "date", "string", "string", "string" });
            _db.AddPrimaryKey("lineitem", new[] { "L_ORDERKEY", "L_PARTKEY", "L_SUPPKEY" });
            _db.LoadFile(@"C:\Users\roman\source\repos\clusterixn\ClusterixN.Database.CustomJoin.Test\bin\data\tbl\empty.tbl", "lineitem");
        }

        [TestMethod]
        public void Join()
        {
            _db.DropTmpRealtions();
            _db.DropTable("NATION");
            _db.DropTable("REGION");
            _db.CreateRelation("NATION", new[] { "N_NATIONKEY", "N_NAME", "N_REGIONKEY", "N_COMMENT" }, new[] { "long", "string", "long", "string" });
            _db.AddIndex("","NATION", new[] { "N_REGIONKEY" });
            _db.LoadFile(@"C:\Users\roman\source\repos\clusterixn\ClusterixN.Database.CustomJoin.Test\bin\data\tbl\nation.tbl", "NATION");
            _db.CreateRelation("REGION", new[] { "R_REGIONKEY", "R_NAME", "R_COMMENT" }, new[] { "long", "string", "string" });
            _db.AddPrimaryKey("REGION", new[] { "R_REGIONKEY" });
            _db.LoadFile(@"C:\Users\roman\source\repos\clusterixn\ClusterixN.Database.CustomJoin.Test\bin\data\tbl\region.tbl", "REGION");
            _db.BlockReaded += (sender, arg) =>
            {
                TestContext.Write(Encoding.UTF8.GetString(arg.Result));
            };
            _db.SelectBlocks("SELECT R_NAME,  N_NATIONKEY, N_NAME, R_REGIONKEY, N_REGIONKEY FROM NATION, REGION WHERE N_REGIONKEY=R_REGIONKEY", 2);
        }

        [TestMethod]
        public void CrossMergeJoin()
        {
            _db.DropTmpRealtions();
            _db.DropTable("A");
            _db.DropTable("B");
            _db.CreateRelation("A", new[] { "A1", "A2" }, new[] { "int", "int" });
            _db.AddIndex("", "A", new[] { "A1", "A2" });
            _db.LoadFile(@"C:\Users\roman\source\repos\clusterixn\ClusterixN.Database.CustomJoin.Test\bin\data\tbl\A.tbl", "A");
            _db.CreateRelation("B", new[] { "B" , "B_N" }, new[] { "int", "int" });
            _db.AddIndex("", "B", new[] { "B" });
            _db.LoadFile(@"C:\Users\roman\source\repos\clusterixn\ClusterixN.Database.CustomJoin.Test\bin\data\tbl\B.tbl", "B");
            _db.BlockReaded += (sender, arg) =>
            {
                var str = Encoding.UTF8.GetString(arg.Result);
                TestContext.Write(str);
            };
            _db.SelectBlocks("SELECT A, A_N, B, B_N FROM A, B WHERE A=B", 100);
        }

        [TestMethod]
        public void LeftOuterJoin()
        {
            _db.DropTmpRealtions();
            _db.DropTable("A");
            _db.DropTable("B");
            _db.CreateRelation("A", new[] { "A", "A_N" }, new[] { "int", "int" });
            _db.AddIndex("", "A", new[] { "A" });
            _db.LoadFile(@"C:\Users\roman\source\repos\clusterixn\ClusterixN.Database.CustomJoin.Test\bin\data\tbl\A.tbl", "A");
            _db.CreateRelation("B", new[] { "B", "B_N" }, new[] { "int", "int" });
            _db.AddIndex("", "B", new[] { "B" });
            _db.LoadFile(@"C:\Users\roman\source\repos\clusterixn\ClusterixN.Database.CustomJoin.Test\bin\data\tbl\B.tbl", "B");
            _db.BlockReaded += (sender, arg) =>
            {
                var str = Encoding.UTF8.GetString(arg.Result);
                TestContext.Write(str);
            };
            _db.SelectBlocks("SELECT A, A_N, B, B_N FROM A LEFT OUTER JOIN B ON A=B", 100);
        }

        [TestMethod]
        public void QueryTest()
        {
            _db.SelectBlocks(@"SELECT
    C_CUSTKEY, 
    O_ORDERKEY
                                FROM
                                    CUSTOMERS AS C LEFT OUTER JOIN
                                    ORDERS AS O
                        ON
                        C_CUSTKEY = O_CUSTKEY", 100);
        }
    }
}
