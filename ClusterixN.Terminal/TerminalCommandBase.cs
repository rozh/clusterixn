#region Copyright
/*
 * Copyright 2017 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

﻿using System;
using System.Collections.Generic;
 using System.Linq;

namespace ClusterixN.Terminal
{
    public class TerminalCommandBase
    {
        public EventHandler ActionEvent;

        public TerminalCommandBase(string command, string description)
        {
            Arguments = new List<CommandArgument>();
            Command = command;
            Description = description;
        }

        public string Command { get; protected set; }

        public string Description { get; protected set; }

        public string RawArgument { get; protected set; }

        public List<CommandArgument> Arguments { get; set; }

        public void DoAction()
        {
            ActionEvent?.Invoke(this, EventArgs.Empty);
        }

        public void ClearArguments()
        {
            foreach (var arg in Arguments)
                arg.Value = string.Empty;

            RawArgument = null;
        }
        
        public void ParseCommandArguments(string command)
        {
            ClearArguments();
            foreach (var argument in Arguments)
            {
                if (CheckArgumentCointains(command, argument))
                {
                    var start = FindArgumentStart(command, argument.Argument);
                    if (start > 0)
                    {
                        argument.IsSet = true;
                        var args = Arguments.Select(a => a.Argument).ToArray();
                        var end = FindArgumentEnd(command, start, args);
                        if (end > 0)
                            argument.Value = command.Substring(start, end - start);
                    }
                }
            }

            RawArgument = command.Substring(Command.Length).Trim();
        }

        private bool CheckArgumentCointains(string command, CommandArgument argument)
        {
            if (command.Contains(argument.Argument)) return true;

            foreach (var argumentSynonim in argument.ArgumentSynonims)
                if (command.Contains(argumentSynonim)) return true;

            return false;
        }

        private int FindArgumentStart(string command, string argument)
        {
            return command.IndexOf(argument, StringComparison.InvariantCultureIgnoreCase) + argument.Length;
        }

        private int FindArgumentEnd(string command, int startIndex, string[] arguments)
        {
            var cmd = command.Substring(startIndex);
            var index = -1;
            foreach (var argument in arguments)
            {
                var ind = cmd.IndexOf(argument, StringComparison.InvariantCultureIgnoreCase);
                if (ind < index || index == -1) index = ind;
            }
            return index > 0 ? startIndex + index : command.Length;
        }
    }
}