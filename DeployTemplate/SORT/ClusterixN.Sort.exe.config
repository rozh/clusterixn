﻿<?xml version="1.0" encoding="utf-8"?>

<configuration>

  <configSections>
    <section name="ListenersConfiguration"
             type="ClusterixN.Configuration.Listener.ListenersConfigSection, ClusterixN.Configuration" />
    <section name="ConnectionsConfiguration"
             type="ClusterixN.Configuration.Connection.ConnectionsConfigSection, ClusterixN.Configuration" />
    <section name="ClusterixN.Infrastructure"
             type="ClusterixN.Infrastructure.ConfigurationElement.BaseServicesConfigurationSectionHandler, ClusterixN.Infrastructure" />
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog" />
  </configSections>

  <ClusterixN.Infrastructure>
    <add key="ClusterixN.Infrastructure.Interfaces.ILogService"
         value="ClusterixN.Infrastructure.Logging.NLogService, ClusterixN.Infrastructure.Logging" />
    <add key="ClusterixN.Infrastructure.Interfaces.IConfigurationService"
         value="ClusterixN.Infrastructure.ConfigurationService, ClusterixN.Infrastructure" />
    <add key="ClusterixN.Infrastructure.Interfaces.IDatabaseService"
         value="ClusterixN.Database.MySQL.DatabaseService, ClusterixN.Database.MySQL" />
  </ClusterixN.Infrastructure>

  <nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        throwExceptions="true">
    <variable name="ApplicationDir" value="."></variable>
    <targets>
      <target name="logfile" xsi:type="File"
              fileName="debug_sort.log"
              layout="[${longdate}] ${level:uppercase=true} ${callsite:skipFrames=1} ${message} ${onexception:\n\rEXCEPTION OCCURRED:${exception:format=type,message,method:maxInnerExceptionLevel=5:innerFormat=shortType,message,method}}"
              archiveEvery="Day"
              archiveDateFormat="yyyyMMdd"
              archiveFileName="archive_sort_{#}.log"
              archiveNumbering="Date"
              encoding="utf-8"
              fileNameKind="Relative"
              maxArchiveFiles="14"
              keepFileOpen="false" />
      <target name="timeCsv" xsi:type="File" fileName="times.csv"
              archiveFileName="times.{####}.csv"
              archiveNumbering="Sequence" >
        <layout xsi:type="CSVLayout">
          <column name="time" layout="${longdate}" />
          <column name="logger" layout="${logger}"/>
          <column name="level" layout="${level}"/>
          <column name="message" layout="${message}" />
        </layout>
      </target>
      <target name="Database" xsi:type="Database" keepConnection="false"
              useTransactions="false"
              dbProvider="System.Data.SQLite.SQLiteConnection, System.Data.SQLite"
              connectionString="Data Source=timeLog.db;Version=3;"
              commandText="INSERT INTO times(Timestamp, Module, Operation, `From`, `To`, QueryId, SubQueryId, RelationId, Duration) 
              VALUES(@Timestamp, @Module, @Operation, @From, @To, @QueryId, @SubQueryId, @RelationId, @Duration)">
        <parameter name="@Timestamp" layout="${event-properties:Time:format=yyyy-MM-dd HH\:mm\:ss.fff}"/>
        <parameter name="@Module" layout="${event-properties:Module}"/>
        <parameter name="@Operation" layout="${event-properties:Operation}"/>
        <parameter name="@From" layout="${event-properties:From}"/>
        <parameter name="@To" layout="${event-properties:To}"/>
        <parameter name="@QueryId" layout="${event-properties:QueryId}"/>
        <parameter name="@SubQueryId" layout="${event-properties:SubQueryId}"/>
        <parameter name="@RelationId" layout="${event-properties:RelationId}"/>
        <parameter name="@Duration" layout="${event-properties:Duration}"/>
      </target>
      <target name="console" xsi:type="Console"
              layout="[${longdate}] ${level:uppercase=true} ${callsite:skipFrames=1} ${message} ${onexception:\n\rEXCEPTION OCCURRED:${exception:format=type,message,method:maxInnerExceptionLevel=5:innerFormat=shortType,message,method}}" />
    </targets>
    <rules>
      <logger name="defaultLogger" minlevel="Info" writeTo="console" />
      <logger name="defaultLogger" minlevel="Info" writeTo="logfile" />
      <logger name="timeLogger" minlevel="Trace" writeTo="Database" />
    </rules>
  </nlog>

  <ConnectionsConfiguration>
    <Connections>
      <add name="DefaultServer" address="localhost" port="1234" />
    </Connections>
  </ConnectionsConfiguration>

  <connectionStrings>
    <add name="Default"
         connectionString="Server=127.0.0.1;Uid=root;Pwd=;Database=sort;DefaultCommandTimeout=86400;AllowUserVariables=true" />
  </connectionStrings>

  <appSettings>
    <add key="ModuleName" value="SORT_1"/>
    <add key="BlockSize" value="1000000"/>
    <add key="DataDir" value="tmp"/>
  </appSettings>

</configuration>