﻿#region Copyright
/*
 * Copyright 2022 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using parsers;

namespace ClusterixN.Database.Calcite.Antlr
{
    public class CalciteQueryRewriter
    {
        public string Rewrite(string query, string schema)
        {
            var inputStream = new AntlrInputStream(query);
            var mySqlLexer = new PlSqlLexer(inputStream);
            var commonTokenStream = new CommonTokenStream(mySqlLexer);
            var mySqlParser = new PlSqlParser(commonTokenStream);
            mySqlParser.AddErrorListener(new BaseErrorListener());

            var rewriter = new TokenStreamRewriter(commonTokenStream);

            var listener = new CalciteQueryListener(rewriter, schema);
            var tree = mySqlParser.sql_script();
            var walker = new ParseTreeWalker();
            walker.Walk(listener, tree);

            return rewriter.GetText();
        }
    }
}
