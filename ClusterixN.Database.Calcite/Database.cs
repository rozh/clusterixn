#region Copyright
/*
 * Copyright 2021 Roman Klassen
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 */
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using ClusterixN.Common;
using ClusterixN.Common.Data.EventArgs;
using ClusterixN.Common.Interfaces;
using ClusterixN.Database.Calcite.Antlr;

namespace ClusterixN.Database.Calcite
{
    public class Database : IDatabase
    {
        protected readonly ILogger Logger;
        private bool _isPause;
        private bool _isStopSelect;
        private readonly object _pauseWaitSyncObject = new object();

        public Database()
        {
            Logger = ServiceLocator.Instance.LogService.GetLogger("defaultLogger");
        }


        public string ConnectionString { get; set; }

        private bool IsPause
        {
            get
            {
                lock (_pauseWaitSyncObject)
                {
                    return _isPause;
                }
            }
            set
            {
                lock (_pauseWaitSyncObject)
                {
                    _isPause = value;
                }
            }
        }
        protected bool IsStopSelect
        {
            get
            {
                lock (_pauseWaitSyncObject)
                {
                    return _isStopSelect;
                }
            }
            set
            {
                lock (_pauseWaitSyncObject)
                {
                    _isStopSelect = value;
                }
            }
        }

        private string GetTableFileName(string tableName)
        {
            var schema = ConnectionStringParser.GetSchema(ConnectionString);
            if (tableName.StartsWith(schema))
            {
                tableName = tableName.Substring(schema.Length + 1, tableName.Length - schema.Length - 1);
            }

            return tableName + ".csv";
        }

        public void LoadFile(string filePath, string tableName)
        {
            using (var input = File.OpenRead(filePath))
            {

                var tablePath = GetTableFilePath(GetTableFileName(tableName));

                using (var bInput = new BufferedStream(input))
                {
                    using (var inputReader = new StreamReader(bInput, Encoding.UTF8))
                    {
                        using (var output = File.OpenWrite(tablePath))
                        {
                            output.Seek(0, SeekOrigin.End);

                            using (var bOutput = new BufferedStream(output))
                            {
                                using (var outputWriter = new StreamWriter(bOutput, Encoding.UTF8))
                                {
                                    var line = inputReader.ReadLine();
                                    if (line == null) return;

                                    if (CheckLoadFormat(line))
                                    {
                                        bInput.Seek(0, SeekOrigin.Begin);
                                        bInput.CopyTo(bOutput);
                                        return;
                                    }

                                    outputWriter.WriteLine(TransformLoadFormat(line));

                                    while ((line = inputReader.ReadLine()) != null)
                                    {
                                        outputWriter.WriteLine(TransformLoadFormat(line));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool CheckLoadFormat(string line)
        {
            return line.Contains("\",\"");
        }

        private string TransformLoadFormat(string line)
        {
            return line.Replace('|', ',');
        }

        public void DissableKeys(string tableName)
        {
            //throw new NotImplementedException();
        }

        public void EnableKeys(string tableName)
        {
            //throw new NotImplementedException();
        }

        public void DropTable(string tableName)
        {
            var filePath = GetTableFilePath(GetTableFileName(tableName));
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public void DropTmpRealtions()
        {
            var dir = ConnectionStringParser.GetWorkingDirectory(ConnectionString);
            foreach (var file in Directory.EnumerateFiles(dir, "*_tmp*"))
            {
                File.Delete(file);
            }
        }

        public void CreateRelation(string name, string[] fields, string[] types)
        {
            var filePath = GetTableFilePath(GetTableFileName(name));
            var header = new StringBuilder();
            for (int i = 0; i < fields.Length; i++)
            {
                header.Append($"{fields[i]}:{StringTypeToColumnType(types[i])}");
                if (i < fields.Length - 1)
                    header.Append(",");
            }
            header.Append(Environment.NewLine);

            File.WriteAllText(filePath, header.ToString());
        }

        private string StringTypeToColumnType(string type)
        {
            var strType = type.ToLower();

            if (strType.Contains("long") || strType.Contains("bigint"))
            {
                return "long";
            }

            if (strType.Contains("int"))
            {
                return "int";
            }

            if (strType.Contains("bool"))
            {
                return "boolean";
            }

            if (strType.Contains("decimal") || strType.Contains("float"))
            {
                return "float";
            }

            if (strType.Contains("time"))
            {
                return "time";
            }
            if (strType.Contains("date"))
            {
                return "date";
            }
            if (strType.Contains("datetime"))
            {
                return "datetime";
            }

            return "string";
        }

        public void AddIndex(string name, string relation, string[] fields)
        {
            //throw new NotImplementedException();
        }

        public void AddPrimaryKey(string relation, string[] fields)
        {
            //throw new NotImplementedException();
        }

        public void QueryIntoRelation(string relationName, string query)
        {
            throw new NotImplementedException();
        }

        private Process StartCalciteProcess(string query)
        {
            var arguments = new StringBuilder(ConnectionString.Length + query.Length + 100);
            var adapterArgs = ConnectionStringParser.GetAdapterRunArgs(ConnectionString);
            if (!string.IsNullOrWhiteSpace(adapterArgs))
            {
                arguments.Append(adapterArgs);
                arguments.Append(" ");
            }

            arguments.Append("\"");
            arguments.Append(ConnectionString);

            var schema = ConnectionStringParser.GetSchema(ConnectionString);
            if (string.IsNullOrWhiteSpace(schema))
            {
                schema = "s";
                arguments.Append(";Schema=" + schema);
            }

            arguments.Append(";Query=");
            arguments.Append(InsertSchemaToQuery(schema, query));
            arguments.Append("\"");

            Logger.Trace($"RUN from {ConnectionStringParser.GetWorkingDirectory(ConnectionString)}: {ConnectionStringParser.GetAdapterFile(ConnectionString)} {arguments}");

            var processInfo = new ProcessStartInfo(ConnectionStringParser.GetAdapterFile(ConnectionString))
            {
                Arguments = arguments.ToString(),
                CreateNoWindow = true,
                StandardErrorEncoding = Encoding.UTF8,
                StandardOutputEncoding = Encoding.UTF8,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                RedirectStandardInput = true,
                WorkingDirectory = ConnectionStringParser.GetWorkingDirectory(ConnectionString)
            };

            return Process.Start(processInfo);
        }

        private string InsertSchemaToQuery(string schema, string query)
        {

            if (query.Contains(";"))
                query = query.Replace(';', ' ');

            foreach (var c in new char[] { '\n', '\r', '\t' })
            {
                query = query.Replace(c, ' ');
            }

            var rewriter = new CalciteQueryRewriter();
            return rewriter.Rewrite(query.ToUpper(), schema);
        }

        /// <summary>
        ///     Выборка по блокам из БД
        /// </summary>
        /// <param name="query">Запрос к БД</param>
        /// <param name="blockSize">Размер блока в строках</param>
        public void SelectBlocks(string query, int blockSize)
        {
#if DEBUG
            Logger.Trace(query);
#endif

            Process process = null;
            try
            {
                process = StartCalciteProcess(query);
                var sb = new StringBuilder();
                string line;
                uint count = 0;
                var sendcount = 0;

                while (!string.IsNullOrWhiteSpace(line = process?.StandardOutput.ReadLine()))
                {
                    if (IsStopSelect)
                    {
                        IsStopSelect = false;
                        process.Kill();
                        process.Close();
                        process.Dispose();
                        return;
                    }

                    sb.Append(line + "\n");
                    count++;

                    if (count % blockSize == 0) //отправка блока
                    {
                        var dest = new char[sb.Length];
                        sb.CopyTo(0, dest, 0, sb.Length);
                        var sendBuffer = Encoding.UTF8.GetBytes(dest);
                        OnBlockReaded(sendBuffer, orderNumber: sendcount++);
                        sb.Clear();

                        WaitPause(process);
                    }
                }

                //отправка последнего блока
                OnBlockReaded(sb.Length > 0 ? Encoding.UTF8.GetBytes(sb.ToString()) : new byte[0], true, sendcount);
            }

            catch (Exception ex)
            {
                Logger.Error("Error:", ex);
                Logger.Trace($"Query: {query}");
            }
            finally
            {
                var error = process?.StandardError.ReadToEnd();
                
                if (!string.IsNullOrWhiteSpace(error)) Logger.Error(error);
                
                process?.Close();
                process?.Dispose();
            }
        }


        /// <summary>
        /// Получение данных из БД по блокам
        /// </summary>
        /// <param name="query">Запрос</param>
        /// <param name="blockSize">Размер блока в строках</param>
        public List<byte[]> Select(string query, int blockSize)
        {
#if DEBUG
            Logger.Trace(query);
#endif
            var result = new List<byte[]>();
            Process process = null;
            try
            {
                var processInfo = new ProcessStartInfo(ConnectionStringParser.GetAdapterFile(ConnectionString))
                {
                    Arguments = $"{ConnectionString} {query}",
                    CreateNoWindow = true,
                    StandardErrorEncoding = Encoding.UTF8,
                    StandardOutputEncoding = Encoding.UTF8,
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true
                };

                process = Process.Start(processInfo);
                var sb = new StringBuilder();
                string line;
                uint count = 0;
                byte[] sendBuffer = null;

                while (!string.IsNullOrWhiteSpace(line = process?.StandardOutput.ReadLine()))
                {
                    if (IsStopSelect)
                    {
                        IsStopSelect = false;
                        process.Kill();
                        process.Close();
                        return new List<byte[]>();
                    }

                    sb.AppendLine(line);
                    count++;

                    if (count % blockSize == 0) //отправка блока
                    {
                        if (sendBuffer != null)
                        {
                            result.Add(sendBuffer);
                        }

                        var dest = new char[sb.Length];
                        sb.CopyTo(0, dest, 0, sb.Length);
                        sendBuffer = Encoding.UTF8.GetBytes(dest);
                        sb.Clear();
                        WaitPause(process);
                    }
                }

                //отправка последнего блока
                if (sendBuffer != null)
                {
                    result.Add(sendBuffer);
                }

                if (sb.Length > 0)
                {
                    result.Add(Encoding.UTF8.GetBytes(sb.Remove(sb.Length - 1, 1).ToString()));
                }
            }

            catch (Exception ex)
            {
                Logger.Error("Error:", ex);
                Logger.Trace($"Query: {query}");
            }
            finally
            {
                process?.Kill();
                process?.Close();
            }

            return result;
        }

        public void ControlSelectBlocks(bool pause)
        {
            IsPause = pause;
        }

        /// <inheritdoc />
        public string CustomCommandQuery(string query)
        {
            throw new NotImplementedException();
        }

        protected void WaitPause(Process process)
        {
            if (IsPause && !IsStopSelect)
            {
                process.StandardInput.Write('s');
                process.StandardInput.Flush();

                while (IsPause && !IsStopSelect)
                {
                    Thread.Sleep(100);
                }

                process.StandardInput.Write('r');
                process.StandardInput.Flush();
            }
        }

        #region Events

        public event EventHandler<SelectResultEventArg> BlockReaded;

        protected void OnBlockReaded(byte[] rows, bool isLast = false, int orderNumber = 0)
        {
            BlockReaded?.Invoke(this, new SelectResultEventArg { Result = rows, IsLast = isLast, OrderNumber = orderNumber });
        }

        #endregion

        public void StopSelectQuery()
        {
            IsStopSelect = true;
        }

        public long GetRelationSize(string relationName)
        {
            try
            {
                var filePath = GetTableFilePath(GetTableFileName(relationName));
                return new FileInfo(filePath).Length;
            }
            catch (Exception)
            {
                //Logger.Error(e);
            }

            return 0;
        }

        public void Dispose()
        {
        }


        private string GetTableFilePath(string tableName)
        {
            return Path.Combine(ConnectionStringParser.GetWorkingDirectory(ConnectionString), tableName);
        }
    }
}