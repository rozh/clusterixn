pipeline {
   agent {label 'docker'}

    environment {
        def ProgVersion="1.13.0.${env.BUILD_NUMBER}"
        def ProgName="ClusterixN"
        def Tag="${ProgName}-${ProgVersion}"
        def ProgDistrName="${ProgName}-${ProgVersion}.tgz"
    }
    
    stages {
        stage('Checkout') {
            steps {
                dir('clusterixn') {
                    git branch: 'master', url: 'https://rozh@bitbucket.org/rozh/clusterixn.git'
                }
                dir('calcirix') {
                    git branch: 'master', url: 'https://github.com/NexoKazan/MySQL_Calcite_Clusterix.git'
                }
            }
        }
        stage('Bump version') {
            steps {
                sh label: 'bump version', script: '''
                cd clusterixn
                sed -i -E "s/.*AssemblyVersion.*/[assembly:\\ AssemblyVersion(\\"${ProgVersion}\\")]/" SharedAssemblyInfo.cs
                sed -i -E "s/.*AssemblyFileVersion.*/[assembly:\\ AssemblyFileVersion(\\"${ProgVersion}\\")]/" SharedAssemblyInfo.cs
                '''
            }
        }
        
        stage('Build Clusterix') {
        
            agent {
                docker {
                    image 'mono:6.12'
                    // Run the container on the node specified at the top-level of the Pipeline, in the same workspace, rather than on a new node entirely:
                    reuseNode true
                    args '-u 0'
                }
            }
            steps {
                sh label: 'build', script: '''
                cd clusterixn
                nuget restore
                MONO_IOMAP=case xbuild /t:Build /p:Configuration="Release" /p:Platform="x64" ClusterixN.sln

                mkdir -p bin/Release/tools/MergeJoinDataLoader
                cp -rf Database/CustomJoin/MergeJoin/MergeJoinDataLoader/bin/Release/ bin/Release/tools/MergeJoinDataLoader
                chown -R 1000:1000 .
                '''
            }
        }

        stage('Build Calcirix') {
            agent {
                docker {
                    image 'maven:3-eclipse-temurin-19'
                    args '-u 0'
                    reuseNode true
                }
            }
            steps {
                sh label: 'build', script: '''
                cd calcirix
                export MAVEN_CONFIG=$(pwd)/.m2
                ./build.sh
                chown -R 1000:1000 .
                '''
            }
        }
        stage('GenDB') {
            agent {
                label 'windows'
            }
            stages {
                stage('Checkout') {
                    steps {           
                        git branch: 'master', url: 'https://github.com/rozh1/tpch_mpp_gen.git'
                    }
                }
                stage('Build') {
                    steps {			
                        bat label: 'build', script: """
                            RMDIR /S /Q dist
                            build.bat
                            """
                    }
                }
            }
            post {
                success {
                    stash name: "gendb_build", includes: "dist/**/*"
                }
            }
        }

        stage('Make artifacts') {
            steps {
                sh label: 'prepare', script: """
                    cp -rf calcirix/target/Calcirix-jar-with-dependencies.jar clusterixn/bin/Release/tools
                    mkdir -p clusterixn/bin/Release/tools/gendb
                """

                dir('clusterixn/bin/Release/tools/gendb'){
                    unstash 'gendb_build'
                }

                sh label: 'archive', script: """
                    tar -czf ${ProgDistrName} --exclude='*.pdb' --exclude='*.xml' --exclude='*.mdb' -C clusterixn/bin/Release/ .
                """
                
                archiveArtifacts artifacts: "${ProgDistrName}", fingerprint: true, followSymlinks: false, onlyIfSuccessful: true
                
            }
        }
      
        stage('Tag') {
            steps {
                withCredentials([string(credentialsId: 'BITBUCKET_TOKEN', variable: 'TOKEN')]) {
                    sh label: 'git-tag', script: """
                        cd clusterixn
                        git config user.name 'jenkins'
                        git config user.email 'jenkins@hpserv.lan'
                        git tag -a ${Tag} -m "[JENKINS] New build ${ProgVersion}"
                        git remote set-url origin https://rozh:${TOKEN}@bitbucket.org/rozh/clusterixn
                        git push origin --tags
                    """
                }
            }
        }

        stage('Upload') {
            steps {
                withCredentials([string(credentialsId: 'BITBUCKET_TOKEN', variable: 'TOKEN')]) {
                    sh label: 'upload', script: """
                        curl -X POST "https://rozh:${TOKEN}@api.bitbucket.org/2.0/repositories/rozh/ClusterixN/downloads" --form files=@"${ProgDistrName}"
                    """
                }
            }
        }
        
        stage('Clean') {
            steps {
                sh "rm ${ProgDistrName}"
                sh "rm -rf clusterixn/bin/Release"
                sh "docker image prune -f"
            }
        }
    }
}